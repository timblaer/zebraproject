Meteor.methods({
    'Dashboard.settings.save': function({widgets, miniWidgets}) {
        const dashboard = Dashboards.findOne();

        Dashboards.update(dashboard._id, {
            ...dashboard,
            widgets,
            miniWidgets
        });
    }
});
