import request from 'request';
import fs from 'fs';

const promisifyCallback = function(resolve, reject) {
    // for Request
    return function(err, res, body) {
        if (err) reject(err);
        resolve(body);
    };
};

Meteor.methods({
    'API.login': ({login, password}) => {
        return new Promise(function(resolve, reject) {
            request.post('https://94.130.104.207:9443/Zebra/webresources/generic/login', {
                rejectUnauthorized: false, // temporary solution
                headers: {
                   'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    login, password
                })
            }, promisifyCallback(resolve, reject));
        });

        /* return axios.post('https://94.130.104.207:9443/Zebra/webresources/generic/login', {
            rejectUnauthorized: false, // temporary solution
            headers: {
               'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                login, password
            })
        })
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });*/
    },

    'API.getDecryptKey': ({otp, sessionId}) => {
        return new Promise(function(resolve, reject) {
            request.post('https://94.130.104.207:9443/ZebraClient/webresources/generic/getDecryptKey', {
                rejectUnauthorized: false, // temporary solution
                headers: {
                   'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    otp, sessionId
                })
            }, promisifyCallback(resolve, reject));
        });
    },

    'API.getPersonalData': ({sessionId, approvalCode}) => {
        return new Promise(function(resolve, reject) {
            request.post('https://94.130.104.207:9443/Zebra/webresources/generic/getPersonalData', {
                rejectUnauthorized: false, // temporary solution
                headers: {
                   'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    sessionId, approvalCode
                })
            }, (err, resp, body) => err ? reject(err) : resolve(body));
        });
    },

    'API.forgetPassword': ({login}) => {
        return new Promise(function(resolve, reject) {
            request.post('https://94.130.104.207:9443/Zebra/webresources/generic/forgetPassword', {
                rejectUnauthorized: false, // temporary solution
                headers: {
                   'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    login
                })
            }, (err, resp, body) => err ? reject(err) : resolve(body));
        });
    },

    'API.changePassword': ({sessionId, cusIdHash, password}) => {
        return new Promise(function(resolve, reject) {
            request.post('https://94.130.104.207:9443/Zebra/webresources/generic/changePassword', {
                rejectUnauthorized: false, // temporary solution
                headers: {
                   'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    sessionId, cusIdHash, password
                })
            }, (err, resp, body) => err ? reject(err) : resolve(body));
        });
    },

    'API.requestOtp': ({sessionId, login}) => {
        return new Promise(function(resolve, reject) {
            request.post('https://94.130.104.207:9443/Zebra/webresources/generic/requestOTP', {
                rejectUnauthorized: false, // temporary solution
                headers: {
                   'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    sessionId, login
                })
            }, (err, resp, body) => err ? reject(err) : resolve(body));
        });
    },

    'formSubmissionMethod': function(formData, captchaData) {
        let verifyCaptchaResponse = reCAPTCHA.verifyCaptcha(this.connection.clientAddress, captchaData);
        if (!verifyCaptchaResponse.success) {
            throw new Meteor.Error(422, 'reCAPTCHA Failed: ' + verifyCaptchaResponse.error);
        }

        // do stuff with your formData

        return true;
    },
    'Log': function(type, message) {
        if (process.env.NODE_ENV != 'production') {
            colors.enabled = true;
            console.log(process.env);
            switch (type) {
               case 'err':
                   message = '  ERROR:' + message;
                   if (true) fs.appendFile('/var/log/zebra.log', `[${moment().format('DD-MM-YYYY HH:mm:ss')}] ${message} \n`);
                   else console.log(message.red);
                   break;
               case 'warn':
                   message = '  WARNING:' + message;
                   if (true) fs.appendFile('/var/log/zebra.log', `[${moment().format('DD-MM-YYYY HH:mm:ss')}] ${message} \n`);
                   else console.log(message.yellow);
                   break;
               default:
                   message = '  INFO:' + message;
                   if (true) fs.appendFile('/var/log/zebra.log', `[${moment().format('DD-MM-YYYY HH:mm:ss')}] ${message} \n`);
                   else console.log(message.blue);
           }
       }
   }
});
