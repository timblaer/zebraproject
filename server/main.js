import {Meteor} from 'meteor/meteor';
import fs from 'fs';
import util from 'util';

Meteor.startup(function() {
    reCAPTCHA.config({
        privatekey: '6LdJTDoUAAAAADKPjfG2M77Owfmdp3QrQTzts-Gd'
    });

/*
    process.stdout.write = (function(write) {
        return function(text, encoding, fd) {
            write.apply(process.stdout, arguments); // write to console

            if (process.env.NODE_ENV == 'production') {
                fs.appendFile(
                    '/var/log/dbs/dbs.log',
                    util.format.apply(process.stdout, arguments) + '\n',
                    'utf-8',
                    function() {
                    }
                );
            }
        };
    })(process.stdout.write);
    */
});


Meteor.publish('Widgets', () => Widgets.find({}));
Meteor.publish('Config', () => Config.find({}));
Meteor.publish('Dashboards', () => Dashboards.find({}));
Meteor.publish('Dictionary', () => Dictionary.find({}));
Meteor.publish('ProductList', () => ProductList.find({}));
Meteor.publish('GeneralData', () => GeneralData.find({}));
