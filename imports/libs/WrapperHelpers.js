import {get} from 'lodash';

const getWidgetName = (data) => get(data, 'name', data); // if data is object - it will get name prop of it

export const widgetName     = ()      => getWidgetName(Template.instance().data);
export const widgetSettings = ()      => Widgets.findOne({name: widgetName(Template.instance().data)}); // user based query
export const widgetStyle    = (prop)  => get(Widgets.findOne({name: widgetName(Template.instance().data)}), `style.${prop}`, '');

export const getWidgetTitle = (widgetName) => get(Config.findOne('widgets'), `${widgetName}.title`, '');
