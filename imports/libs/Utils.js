import {get, isObject, isEmpty, isString} from 'lodash';
import Collections from '/lib/collections';

export function packRows(array) {
    const result = [[]];
    let currow = 0;
    let curlen = 0;

    for (let i = 0; i < array.length; i++) {
        let curWidth = get(array[i], 'style.width', 4);

        if (curWidth + curlen <= 12) {
            curlen += curWidth;
            result[currow].push(array[i]);
        } else {
            result.push([]);
            curlen = curWidth;
            result[++currow].push(array[i]);
        }
    }

    return result;
}

export function objToArray(object) {
    const arr = [];

    Object.keys(object).forEach((key) => {
        arr.push({
            ...object[key],
            newProp: key
        });
    });

    return arr;
}

export function processQuery(query) {
    const {
        collection,
        method,
        body,
        additional,
        props,
        process
    } = query;

    let data = null;

    data = Collections[collection].find(body, additional);

    if (method == 'findOne') {
        data = get(data.fetch(), props, null);
    }

    return AppEvents.run(process, data);
}

export function untemplateString(string, context = null) {
    if (isEmpty(string) || !isString(string)) {
        return string;
    }

    return string.replace(/\${.*?}/g, (rawEntry) => {
        const entry = rawEntry.replace('${', '').replace('}', '');

        if (context !== null) {
            return get(context, entry, entry);
        }

        const splitted   = entry.split(':');

        if (splitted[0] == 'Dict') {
            const CurrentLang = AppState.get('Dictionary.CurrentLanguage');
            const prop = splitted[1];

            return dataGetter(`Dictionary:${prop}:${CurrentLang}`, AppState);
        }

        const stateValue = AppState.get(splitted[0]);

        if (splitted.length > 1) {
            return get(stateValue, splitted.slice(1), entry);
        }

        return stateValue;
    });
}

const mongoParser = function(json) {
    for (let key in json) {
        if (isObject(json[key])) {
            if (json[key]['$date'] !== undefined) {
                json[key] = new Date(json[key]['$date']);
            } else if (json[key]['$oid'] !== undefined) {
                json[key] = json[key]['$oid'];
            } else mongoParser(json[key]);
        }
    }
};

export function mongoParse(rawJson) {
    try {
        const json = JSON.parse(rawJson);
        mongoParser(json);

        return json;
    } catch (e) {
        return rawJson;
    }
}

export function dataGetter(prop, source) {
    const params    = prop.split(':');
    const props     = untemplateString(prop).replace(`${params[0]}:`, '').replace(/:/g, '.');
    const value     = source.get(params[0]);

    if (params.length > 1) {
        return get(value, props, value);
    }

    return value;
}
