import {coll} from './ResponsiveTable/data.js'


Template.SimpleTable.onCreated(function() {
    this.DB = coll();
});


Template.SimpleTable.helpers({
    titleInfo: function() {
        return Template.instance().DB.settings.columns;
    },
    getTitle: function() {
        return Template.instance().DB.settings.columns;
    },
    isDataCol: function(col) {
        return col.type == 'data';
    },
    columns: function() {
        return eval(Template.instance().DB.settings.query);
    },
    getProp: function(doc, col) {
        return doc[col.value];
    },
    getClass: function(doc) {
        return doc.class;
    }
});
