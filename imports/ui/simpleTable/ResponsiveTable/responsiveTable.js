function delete_row(row) {
    let box = $('#mb-remove-row');
    box.addClass('open');

    box.find('.mb-control-yes').on('click', function() {
        box.removeClass('open');
        $('#'+row).hide('slow', function() {
          // base logic. Delete row
        });
    });
    box.find('.mb-control-close').on('click', function() {
        box.removeClass('open');
        });
}

Template.ResponsiveTable.helpers({
    titleInfo: function() {
            return Template.instance().data.settings.columns;
    },
    getTitle: function() {
        return Template.instance().data.settings.columns;
    },
    isDataCol: function(col) {
        return col.type == 'data';
    },
    columns: function() {
        return eval(Template.instance().data.settings.query);
    },
    getProp: function(doc, col) {
        return doc[col.value];
    }
});


Template.ResponsiveTable.events({
  'click .table-delete-row-btn': (event) => {
    delete_row(event.target.dataset.id);
  }
});
