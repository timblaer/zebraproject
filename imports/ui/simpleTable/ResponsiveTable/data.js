
export function coll() {
    let a = {};
    a['settings']={};
    a.settings['columns'] = [];
    a.settings.columns.push({
        title: 'Account Number',
        type: 'data',
        value: 'AccountNumber'
    });
    a.settings.columns.push({
        title: 'IBAN',
        type: 'data',
        value: 'IBAN'
    });
    a.settings.columns.push({
        title: 'CurrencyId',
        type: 'data',
        value: 'CurrencyId'
    });
    a.settings.columns.push({
        title: 'Acc Product',
        type: 'data',
        value: 'AccProduct'
    });
    a.settings.columns.push({
        title: 'Open Actual Bal',
        type: 'data',
        value: 'OpenActualBal'
    });

    a.settings.query = 'ProductList.findOne().Accounts';
    return a;
}
