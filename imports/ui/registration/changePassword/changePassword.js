import {mongoParse} from '/imports/libs/Utils';


Template.changePassword.onRendered(function() {
    let newPassword    = $('#newPassword').val();
    let repeatNewPassword = $('#repeatNewPassword').val();
    if (newPassword.length>=1 && repeatNewPassword.length>=1) {
        $('#input').prop('disabled', false );
    } else $('#input').prop('disabled', true );
});

Template.changePassword.helpers({
    isFirstTime: function() {
        return AppStorage.get('errorCode') != 'FirstTimeLogin';
    }
});

Template.changePassword.events({
    'submit .changeOk': (e)=>{
        let newPassword = $('#newPassword').val();
        let repeatNewPassword= $('#repeatNewPassword').val();
        let sessionId = AppStorage.get('user.sessionID');
        let cusIdHash = AppStorage.get('user.cusIdHash');

        if (newPassword == repeatNewPassword && newPassword.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/i)) {
            e.preventDefault();

            let captchaData = grecaptcha.getResponse();

            Meteor.call('formSubmissionMethod', {}, captchaData, function(error) {
                grecaptcha.reset();
                if (!error) {
                    API.changePassword({
                        sessionId: sessionId,
                        cusIdHash: cusIdHash,
                        password: newPassword
                    }).then((resp)=>{
                        resp = mongoParse(resp);
                        if (resp.errorCode == 'OK') {
                            AppStorage.set('user.sessionID', resp.sessionId);

                            $('#mb-error').addClass('open');
                        }
                    });
                }
            });
        }

        return false;
    },
    'click .cancel': ()=>{
        history.back();
    },
    'keyup .form-control': ()=>{
        let newPassword    = $('#newPassword').val(); // use const if variable is not changed (everywhere)
        let repeatNewPassword = $('#repeatNewPassword').val();
        if (newPassword.length>=1 && repeatNewPassword.length>=1) {
            $('#input').prop('disabled', false );
        } else $('#input').prop('disabled', true );
    },
    'click .mb-control-close': ()=>{
        Router.go('/dashboard');
    }
});
