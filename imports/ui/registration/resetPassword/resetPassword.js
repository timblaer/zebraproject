Template.resetPassword.events({
    'submit .resetOk': (e, i)=>{
        let login = $('#login').val();
        e.preventDefault();

        let captchaData = grecaptcha.getResponse();

        Meteor.call('formSubmissionMethod', {}, captchaData, function(error) {
            grecaptcha.reset();
            if (!error) {
                API.forgetPassword({login: login}).then((resp)=>{
                    if (resp == '"OK"') {
                    $('#mb-notification').addClass('open');
                    }
                });
            } else {
                $('#mb-error').addClass('open');
            }
        });

        return false;
    },
    'click .cancel': (e, i)=>{
        Router.go('/login');
    },
    'click #goLogin': (e)=>{
        Router.go('/login');
    },
    'click #closeWindow': (e)=>{
        $('#mb-error').removeClass('open');
    }
});
