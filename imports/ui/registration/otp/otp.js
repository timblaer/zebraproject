import {mongoParse} from '/imports/libs/Utils';
import {get} from 'lodash';

Template.OtpPage.onCreated(function() {
    AppState.set('CountSendOtp', 0);
    AppStorage.set('user.otp', false);
});


Template.OtpPage.events({
    'submit .OtpOk': (e, i)=>{
        let otp = $('#otp').val();
        let sessionId = AppStorage.get('user.sessionID');

        API.getDecryptKey({sessionId, otp})
        .then((resp) => {
            resp = mongoParse(resp);
            if (resp.errorCode == 'OK') {
                const approvalCode = resp.approvalCode;
                AppStorage.set('user.approvalCode', resp.approvalCode);
                AppStorage.set('user.cryptoKey', resp.cryptoKey);
                return API.getPersonalData({sessionId, approvalCode});
            } else Router.go('/otp');
        })
        .then((resp) => {
            resp = mongoParse(resp);
            if (AppStorage.get('errorCode') == 'FirstTimeLogin') {
                Router.go('/changePassword');
            }
            if (get(resp, 'ErrorCode', 'error') == 'OK') {
                AppStorage.set('user.otp', true);
                AppStorage.set('user.personalData', resp.PersonalData);
                Router.go('/dashboard');
        } else {
            $('#mb-error').addClass('open');
        }
        });
        return false;
    },
    'click #SendAgain': ()=>{
        let Count = AppState.get('CountSendOtp');
        if (Count < 3) {
            let sessionId = AppStorage.get('user.sessionID');
            let login = AppStorage.get('user.login');
            API.requestOtp({sessionId, login}).then((resp)=>{});
            Count++;
            AppState.set('CountSendOtp', Count);
        } else {
            $('#SendAgain').prop('disabled', true);
        }
    },
    'click .mb-control-close': ()=>{
        $('#mb-error').removeClass('open');
    }
});
