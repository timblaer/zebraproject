import {mongoParse} from '/imports/libs/Utils';

Template.loginPage.onRendered(function() {
    AppState.set('countEnters', 0);
    let login    = $('#username').val();
    let password = $('#password').val();
    if (password.length>=1 && login.length>=1) {
        $('#input').prop('disabled', false );
    } else $('#input').prop('disabled', true );
});

Template.loginPage.helpers({
    'countEnters': function() {
        if (AppState.get('countEnters')>2) {
return true;
}
        return false;
    }
});


Template.loginPage.events({
    'submit .LogIn': ()=>{
        let login    = $('#username').val();
        let password = $('#password').val();

        event.preventDefault();


        if (AppState.get('countEnters')>2) {
            let captchaData = grecaptcha.getResponse();
            Meteor.call('formSubmissionMethod', {}, captchaData, function(error) {
                grecaptcha.reset();
                if (!error) {
                    API.login({
                        login, password
                    }).then((resp)=>{
                        resp = mongoParse(resp);

                        if (resp.errorCode !== 'Exception') {
                            AppStorage.set('user.sessionID', resp.sessionID);
                            AppStorage.set('user.login', login);
                            AppStorage.set('user.cusIdHash', resp.cusIdHash);
                            AppStorage.set('user.errorCode', resp.errorCode);
                            Router.go('/otp');
                        } else {
                            $('#mb-error').addClass('open');
                        }
                    });
                }
            });
        } else {
            API.login({
                login, password
            }).then((resp)=>{
                if (resp) {
                    resp = mongoParse(resp);
                    if (resp.errorCode !== 'Exception') {
                        AppStorage.set('user.sessionID', resp.sessionID);
                        AppStorage.set('user.login', login);
                        AppStorage.set('user.cusIdHash', resp.cusIdHash);
                        AppStorage.set('user.errorCode', resp.errorCode);
                        Router.go('/otp');
                    } else {
                        $('#mb-error').addClass('open');
                        AppState.set('countEnters', AppState.get('countEnters')+1);
                    }
                }
            });
        }


        return false;
    },
    'click .mb-control-close': ()=>{
        $('#mb-error').removeClass('open');
        $('#username').innerHTML ='';
        $('#password').innerHTML ='';
    },
    'keyup .form-control': ()=>{
        let login    = $('#username').val();
        let password = $('#password').val();
        if (password.length>=1 && login.length>=1) {
            $('#input').prop('disabled', false );
        } else $('#input').prop('disabled', true );
    }

});
