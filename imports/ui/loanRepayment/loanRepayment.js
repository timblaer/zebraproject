import {get} from 'lodash';

Template.LoanRepayment.onCreated(function() {
    AppState.set('Repayment.repaymentType', get(Template.instance().data, `options.inputs[2].options.optionsList[0]`, undefined));
});

Template.LoanRepayment.events({
    'submit .LoanForm': () =>{
        return false;
    },
    'click .reset': () =>{
        let reject = confirm('Are you sure?');
        if (reject) history.back();
    },
    'change .repayment': () =>{
        AppState.set('Repayment.repaymentType', $('.repayment').val());
    }
});

Template.LoanRepayment.helpers({
    'getDB': (index) =>{
        const inputData = get(Template.instance().data, `options.inputs[${index}]`, undefined);

        if (index == 1) {
            return {
                ...inputData,
                options: {
                    ...inputData.options,
                    result: get(AppState.get('CurrentLoan'), 'ContractNumber', 'No Contract Number')
                }
            };
        }

        if ((AppState.get('Repayment.repaymentType') ==
            get(Template.instance().data, `options.inputs[2].options.optionsList[0]`, undefined)) &&
            (index == 3)) {
                return {
                    ...inputData,
                    options: {
                        ...inputData.options,
                        readonlyMode: true,
                        result: parseFloat(AppState.get('CurrentLoan').PaymentAmount)
                    }
                };
        }

        return inputData;
    }
});
