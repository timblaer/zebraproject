import {page_content_onresize} from '/client/lib/actions.js';

Template.MessagesTopBar.events({
    'click .content-frame-right-toggle'() {
        $('.content-frame-right').is(':visible')
        ? $('.content-frame-right').hide()
        : $('.content-frame-right').show();
        page_content_onresize();
    }
});
