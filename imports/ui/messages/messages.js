import {layoutLoaded} from '/client/lib/actions.js';

Template.Messages.onRendered(function() {
    $('.messages .item').each(function(index) {
        let elm = $(this);
        setInterval(function() {
            elm.addClass('item-visible');
        }, index*300);
    });
    layoutLoaded();
});
