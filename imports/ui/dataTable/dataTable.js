import {get} from 'lodash';


Template.DataTable.onCreated(function() {
    this.mode = new ReactiveVar(1);
    this.sort = new ReactiveVar('');
    AppState.set(Template.instance().data.options.query.dataview, new AppQuery(
        Template.instance().data.options.query
    ));
});

Template.DataTable.onRendered(function() {
    AppState.set('onRendered', true);
});


Template.DataTable.helpers({
    titleInfo: function() {
        return Template.instance().data.options.columns;
    },
    isDataCol: function(col) {
        return col.type == 'data';
    },
    isAction: function(type) {
        return type != 'data';
    },
    hasData: function(doc) {
        return doc[Template.instance().data.options.columns[0].value] != '&nbsp;';
    },
    columns: function() {
        const dataview  = Template.instance().data.options.query.dataview;
        let   documents = AppState.get(dataview);
        if (AppState.get('TotalInfoMode') && AppState.get('onRendered')) {
            if (get($('tbody')[0], 'childNodes', false))$('tbody')[0].childNodes[2].className ='DataTableRow info';
            AppState.set('active.row', AppState.get('TotalInfoMode'));
            if (Template.instance().data.options.events.click) {
                AppEvents.run(Template.instance().data.options.events.click, Template.instance().data.options);
            }
        } else {
            AppState.set('active.row', '');
            if ($('.info').hasClass('info'))$('.info').removeClass('info');
        }
        if (Template.instance().sort.get() != '') {
            documents.sort(function(a, b) {
                a = a[Template.instance().sort.get()];
                b = b[Template.instance().sort.get()];
                if (parseFloat(a)) {
                a = parseFloat(a);
                b = parseFloat(b);
}
                if (a>b) return Template.instance().mode.get();
                else     return -Template.instance().mode.get();
            });
        }
        documents = AppEvents.run('ArraySearch', AppState.get('search'), documents);
        if ( AppState.get('LastTransactionsByPeriodDateFrom')) {
            documents = documents.filter(function(elem) {
            if (new Date(elem.Date) >= new Date(AppState.get('LastTransactionsByPeriodDateFrom'))
            &&  new Date(elem.Date) <= new Date(AppState.get('LastTransactionsByPeriodDateTo'))) {
                return true;
            }
            return false;
        });
}
        let Pagination = AppState.get(dataview + '.pagination');
        const current = get(Pagination, 'current', 0);
        const dataPerPage = get(Pagination, 'dataPerPage', 5);
        if (documents) {
                documents = documents.slice(current*dataPerPage, (current+1)*dataPerPage);
                for (let i = documents.length; i < 5; i++) {
                    let pos = {};
                    if (!Template.instance().data.options.IsId) {
                        Template.instance().data.options.IsId=2;
                    }
                    if (Template.instance().data.options.IsId == 1) {
                        pos[Template.instance().data.options.columns[1].value]='&nbsp;';
                    } else {
                        pos[Template.instance().data.options.columns[0].value]='&nbsp;';
                    }
                    documents.push(pos);
                }
                return documents;
            }
    },
    IsId: function(col) {
        if (col.IsId) return col.IsId;
        return false;
    },
    getProp: function(doc, col) {
        return doc[col.value];
    }
});


Template.DataTable.events({
    'click .sort_btn': (event, instance) => {
        if (event.target.id != instance.sort.get()) {
            $('#' + instance.sort.get()).addClass('sorting');
            $('#' + instance.sort.get()).removeClass('sorting_asc');
            $('#' + instance.sort.get()).removeClass('sorting_desc');
            instance.sort.set(event.target.id);
            instance.mode.set(1);
            $('#' + instance.sort.get()).addClass('sorting_asc');
            $('#' + instance.sort.get()).removeClass('sorting');
            $('#' + instance.sort.get()).removeClass('sorting_desc');
        } else {
            if (instance.mode.get() == true) {
                instance.mode.set(-1);
                $('#'+instance.sort.get()).addClass('sorting_desc');
                $('#'+instance.sort.get()).removeClass('sorting_asc');
            } else {
                instance.mode.set(1);
                $('#' + instance.sort.get()).addClass('sorting_asc');
                $('#'+instance.sort.get()).removeClass('sorting_desc');
            }
        }
    },
    'click .DataTableRow': (event, instance)=>{
        if (!Template.instance().data.options.IsId) {
            Template.instance().data.options.IsId=2;
        }
        if (get(event.currentTarget.childNodes[2 + (Template.instance().data.options.IsId - 1) * 5], 'innerText', '') == '') {
            return false;
        }
        if ($('.info').hasClass('info'))$('.info').removeClass('info');
        event.currentTarget.className = 'DataTableRow info';
        if (get(event.currentTarget.childNodes[2 + (Template.instance().data.options.IsId - 1) * 5], 'innerText', false)) {
            AppState.set('active.row', event.currentTarget.childNodes[2 + (Template.instance().data.options.IsId - 1) * 5].innerText);
        }

        if (!AppState.get('active.row').length < 3) {
            if (AppState.get('TotalInfoMode')) {
                event.currentTarget.className = 'DataTableRow';
                $('tbody')[0].childNodes[2].className ='DataTableRow info';
                AppState.set('active.row', $('tbody')[0].childNodes[2].childNodes[2 + (Template.instance().data.options.IsId - 1) * 5].innerText);
            }
        }

        if (get(Template.instance().data.options.events, 'click', false)) {
            AppEvents.run(Template.instance().data.options.events.click, Template.instance().data.options);
        }

        if (!get(Template.instance().data.options.events, 'current', true) ) {
            event.currentTarget.className = 'DataTableRow';
            AppState.set('active.row', '');
        }
    }
});

Template.DataTable.onDestroyed(function() {
    AppState.set('active.row', '');
    AppState.set('TotalInfoMode', false);
    AppState.set('search', '');
});
