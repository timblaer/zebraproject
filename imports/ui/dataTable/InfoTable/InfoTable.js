import {get} from 'lodash';
AppState.set('money', [
    {
        'currency': 'USD',
        'value': 1.7
    },
    {
        'currency': 'EUR',
        'value': 2
    },
    {
        'currency': 'GBP',
        'value': 2.4
    },
    {
        'currency': 'AZN',
        'value': 1
    },
    {
        'currency': 'RUB',
        'value': 34.12
    }
]);

AppState.set('overallloans', {
    TotalDebt: 0
});


Template.InfoTable.onCreated(function() {
    AppState.set(Template.instance().data.options.query.props, new AppQuery(Template.instance().data.options.query));
});


Template.InfoTable.helpers({
    caption: function() {
        return Template.instance().data.options.caption;
    },
    TotalSetting: function() {
        let TotalData= AppState.get(Template.instance().data.options.AppStateName);
        let DataBase = AppState.get(Template.instance().data.options.query.props);
        return  AppEvents.run(Template.instance().data.options.generalInformation, TotalData, DataBase);
    },
    Menu: function() {
        return AppState.get('MenuBtnOpen');
    },
    items: function() {
        return Template.instance().data.options.items;
    },
    data: function() {
        return Template.instance().data.options.data;
    },
    active() {
        if (AppState.get('active.row')) return true;
        return false;
    },
    haveOperation: function() {
        return get(AppState.get(Template.instance().data.options.current), 'operations', true);
    }
});


Template.InfoTable.events({
    'click .operations': ()=>{
            AppState.set('MenuBtnOpen', true);
    },
    'click .accountsInfo': ()=>{
            AppState.set('MenuBtnOpen', false);
    }
});

Template.InfoTable.onDestroyed(function() {
    AppState.set('MenuBtnOpen', false);
});
