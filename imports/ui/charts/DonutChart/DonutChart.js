

Template.DonutChart.onRendered(function() {
    const TemplateName = Template.instance().data.name;
    const parent = Template.instance().data.parent;
    const ID = `DonutChart.${TemplateName}.${parent}`;
    const colors = [];

    for (let color in ColorDict) {
        if (object.hasOwnProperty(color)) {
            colors.push(ColorDict.color);
        }
    }

    AppState.addCallback((id) => {
        if (id === ID) {
            $('#morris-line-example').empty();
            Morris.Donut({
                element: 'morris-donut-example',
                data: [],
                colors: colors
            }).select(0);
        }
    });

    AppState.set(ID, new AppQuery({

    }));
});
