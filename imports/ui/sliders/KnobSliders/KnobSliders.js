Template.KnobSliders.onRendered(function() {
    if ($('.knob').length > 0) {
        $('.knob').knob();
    }
});


Template.KnobSliders.helpers({
    KnobSlidersRow1() {
        return [
            {
                'events': {

                },

                'options': {
                    'name': 'Disable display input',
                    'class': 'knob',
                    'data_width': '150',
                    'data_fgColor': '#33414E',
                    'data_displayInput': 'false',
                    'value': '35'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': '"cursor" mode',
                    'class': 'knob',
                    'data_width': '150',
                    'data_cursor': 'true',
                    'data_fgColor': '#81C500',
                    'data_displayInput': 'true',
                    'value': '29'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': 'Display previous value',
                    'class': 'knob',
                    'data_width': '150',
                    'data_min': '-100',
                    'data_fgColor': '#FD421C',
                    'data_displayPrevious': 'true',
                    'value': '44'
                }
            }
        ];
    },

    KnobSlidersRow2() {
        return [
            {
                'events': {

                },

                'options': {
                    'name': 'Angle offset',
                    'class': 'knob',
                    'data_width': '150',
                    'data_angleOffset': '90',
                    'data_linecap': 'round',
                    'data_fgColor': '#61C0E6',
                    'value': '35'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': 'Angle offset and arc',
                    'class': 'knob',
                    'data_width': '150',
                    'data_cursor': 'true',
                    'data_fgColor': '#FEC558',
                    'value': '29'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': '5-digit values, step 1000',
                    'class': 'knob',
                    'data_width': '150',
                    'data_min': '-15000',
                    'data_max': '15000',
                    'data_displayPrevious': 'true',
                    'data_fgColor': '#81C500',
                    'data_step': '1000',
                    'value': '-11000'
                }
            }
        ];
    }
});
