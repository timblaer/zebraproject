Template.RangeSliders.onRendered(function() {
    if ($('.defaultSlider').length > 0) {
        $('.defaultSlider').each(function() {
            let rsMin = $(this).data('min');
            let rsMax = $(this).data('max');

            $(this).rangeSlider({
                bounds: {min: 1, max: 200},
                defaultValues: {min: rsMin, max: rsMax}
            });
        });
    }

    if ($('.dateSlider').length > 0) {
        $('.dateSlider').each(function() {
            $(this).dateRangeSlider({
                bounds: {min: new Date(2012, 1, 1), max: new Date(2015, 12, 31)},
                defaultValues: {min: new Date(2012, 10, 15), max: new Date(2014, 12, 15)}
            });
        });
    }

    if ($('.rangeSlider').length > 0) {
        $('.rangeSlider').each(function() {
            let rsMin = $(this).data('min');
            let rsMax = $(this).data('max');

            $(this).rangeSlider({
                bounds: {min: 1, max: 200},
                range: {min: 20, max: 40},
                defaultValues: {min: rsMin, max: rsMax}
            });
        });
    }

    if ($('.stepSlider').length > 0) {
        $('.stepSlider').each(function() {
            let rsMin = $(this).data('min');
            let rsMax = $(this).data('max');

            $(this).rangeSlider({
                bounds: {min: 1, max: 200},
                defaultValues: {min: rsMin, max: rsMax},
                step: 10
            });
        });
    }
});

Template.RangeSliders.helpers({
    RangeSliders() {
        return [
            {
                'events': {

                },

                'options': {
                    'name': 'Default',
                    'class': 'defaultSlider',
                    'data_min': '20',
                    'data_max': '180'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': 'Date slider',
                    'class': 'dateSlider',
                    'data_min': '20',
                    'data_max': '180'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': 'Range',
                    'class': 'rangeSlider',
                    'data_min': '20',
                    'data_max': '180'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': 'Step',
                    'class': 'stepSlider',
                    'data_min': '20',
                    'data_max': '180'
                }
            }
        ];
    }
});
