Template.IonRangeSliders.onRendered(function() {
  $('.ise_default').ionRangeSlider();
  $('.ise_min_max').ionRangeSlider({
        min: 100,
        max: 1000,
        from: 550
    });
  $('.ise_prefix').ionRangeSlider({
        type: 'double',
        grid: true,
        min: 0,
        max: 1000,
        from: 200,
        to: 800,
        prefix: '$'
    });
  $('.ise_step').ionRangeSlider({
        type: 'double',
        grid: true,
        min: 0,
        max: 10000,
        from: 3000,
        to: 7000,
        step: 250
    });
  $('.ise_custom_values').ionRangeSlider({
        grid: true,
        from: 3,
        values: [
            'January', 'February', 'March',
            'April', 'May', 'June',
            'July', 'August', 'September',
            'October', 'November', 'December'
        ]
    });
  $('.ise_decorate').ionRangeSlider({
        type: 'double',
        min: 100,
        max: 200,
        from: 145,
        to: 155,
        prefix: 'Weight: ',
        postfix: ' million pounds',
        decorate_both: false
    });
  $('.ise_disabled').ionRangeSlider({
        min: 0,
        max: 100,
        from: 30,
        disable: true
    });
});

Template.IonRangeSliders.helpers({
    IonRangeSliders() {
        return [
            {
                'events': {

                },

                'options': {
                    'name': 'Default',
                    'description': 'Start without params',
                    'ClassAndName': 'ise_default'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': 'Min-Max',
                    'description': 'Set min value, max value and start point',
                    'ClassAndName': 'ise_min_max'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': 'Prefix',
                    'description': 'Showing grid and adding prefix "$"',
                    'ClassAndName': 'ise_prefix'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': 'Step',
                    'description': 'Step 250',
                    'ClassAndName': 'ise_step'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': 'Custom Values',
                    'description': 'One more example with strings',
                    'ClassAndName': 'ise_custom_values'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': 'Decorate',
                    'description': 'Taking care about how from and to values connect? Use decorate_both option',
                    'ClassAndName': 'ise_decorate'
                }
            },

            {
                'events': {

                },

                'options': {
                    'name': 'Disabled',
                    'description': 'You can lock your slider, by using disable option',
                    'ClassAndName': 'ise_disabled'
                }
            }
        ];
    }
});
