import {get} from 'lodash';

Template.MiniMoneyTransferComponent.onCreated(function() {
    AppState.set('AccountsForTransaction', new AppQuery(
        Template.instance().data.options.query
    ));
});

Template.MiniMoneyTransferComponent.events({
    'submit .MiniMoneyTransferComponent': ()=>{
        return false;
    },
    'click .reset': () =>{ //   remove unused vars everywhere
        let reject = confirm('Are you sure?');
        if (reject) history.back();
    }
});

Template.MiniMoneyTransferComponent.helpers({
    'getDB': (index)=>{
        const inputData = get(Template.instance().data, `options.inputs[${index}]`, undefined);
        let documents = AppState.get('AccountsForTransaction');
        let accouunList = [];
        for (let i = 0; i < documents.length; i++) {
            accouunList.push(documents[i].AccountId);
        }
        if (!index) {
            return {
                ...inputData,
                options: {
                    ...inputData.options,
                    col_md: 9,
                    optionsList: accouunList
                }
            };
        }

        else if (index == 1) {
            return {
                    ...inputData,
                    options: {
                        ...inputData.options,
                        col_md: 9,
                        result: AppState.get('CurrentDeposit').ContrID
                    }
            };
        }

        else {
            return {
                    ...inputData,
                    options: {
                        ...inputData.options,
                        col_md: 9
                    }
            };
        }
    }
});
