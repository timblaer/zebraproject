import {get} from 'lodash';

Template.Exchange.onCreated(function() {
    AppState.set('currentCurrency', 'AZN');
    AppState.set('currencyValue', 0);
});

Template.Exchange.events({
    'keyup .exchangeResultView': (event) =>{
        AppState.set('currencyValue', event.target.value);
        AppState.set('currentCurrency', $(event.target).parent().children('span').text());
    },

    'click .exchangeResultView': (event) =>{
        if ($(event.target).val() == 0) $(event.target).val('');
    },

    'focusout .exchangeResultView': (event) =>{
        if (!$(event.target).val()) $(event.target).val(0);
    }
});

Template.Exchange.helpers({
    'getDB': (index, currency) => {
        let exchangeСoeffByAZN = AppState.get('ExchangeRate');

        exchangeСoeffByAZN['AZN'] = {'sell': '1', 'buy': '1'};

        if (AppState.get('currencyValue') < 0) AppState.set('currencyValue', 0);

        let result = AppState.get('currencyValue') * (1 / exchangeСoeffByAZN[AppState.get('currentCurrency')]['buy']) * exchangeСoeffByAZN[currency]['sell'];

        if (currency == AppState.get('currentCurrency')) result = AppState.get('currencyValue');

        const inputData = get(Template.instance().data, `options.inputs[${index}]`, undefined);

        const resultInput = {
            ...inputData,
            options: {
                ...inputData.options,
                col_md: 12,
                leftText: currency,
                result
            }
        };

        return resultInput;
    },

    'currencies': () =>{
        let arr = Object.keys(AppState.get('ExchangeRate'));
        arr.unshift('AZN');
        return arr;
    }
});
