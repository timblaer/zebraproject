Template.GeneralInformation.helpers({
    userName: function() {
        let personalData = AppStorage.get('user.personalData');
        return personalData.Name.FullName.EN;
    },
    clientID: function() {
        let personalData = AppStorage.get('user.personalData'); // unneeded variable
        return personalData.CustomerId;
    }
});


Template.GeneralInformation.events({
    'click #changePasword': ()=>{
        Router.go('/changePassword');
    }
});
