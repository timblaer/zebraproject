

Template.Forms.helpers({
    templateList() {
         return FormsData.options.inputs.map((x) => ({
             ...x,
             parent: FormsData.name
         })
        );
    }
});

Template.Forms.events({
    'submit #someid'(event) {
        let inputsArray = FormsData.options.inputs;

        const inputs = {};

        for (let i = 0; i < inputsArray.length; i++) {
            let templateType = inputsArray[i].type;
            let templateName = inputsArray[i].name;

            if (templateType == 'FormsInputTags') {
                inputs[templateName] = event.target[templateName].value.split(',');
            } else if (templateType == 'FormsInputCheckbox') {
                inputs[templateName] = $(`#input-${FormsData.name}-${templateName}`).parent().hasClass('checked');
            } else inputs[templateName] = event.target[templateName].value;
        }

        AppEvents.run(FormsData.events.submit, inputs);

        return false;
    },

    'reset #someid'() {
        $('#someid .tagsinput').val().split(',').forEach((x) => $('#someid .tagsinput').removeTag(x));
        $('#someid .file-input-name').text('');
        $('#someid .checked').each(function() {
            $(this).removeClass('checked');
        });
    }
});
