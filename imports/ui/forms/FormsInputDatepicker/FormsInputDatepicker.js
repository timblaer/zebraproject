Template.FormsInputDatepicker.onRendered(function() {
        $('.datepicker').datepicker({format: 'yyyy-mm-dd'});
});

Template.FormsInputDatepicker.events({
    'change .Datepicker': (event, instance) => {
        AppEvents.run(instance.data.events.change, event.target.value);
    },

    'keyup .Datepicker': (event, instance) => {
        AppEvents.run(instance.data.events.keyup, event.target.value);
    },

    'keydown .Datepicker': (event, instance) => {
        AppEvents.run(instance.data.events.keydown, event.target.value);
    },

    'click .Datepicker': (event, instance) => {
        AppEvents.run(instance.data.events.click, event.target.value);
    }
});
