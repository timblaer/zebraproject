import {get} from 'lodash';

Template.FormsInputBasic.events({
    // "change .Basic": (event, instance) => {
    //     AppEvents.run(instance.data.events.change, event.target.value);
    // },
    //
    'keyup .Basic': (event, instance) => {
        if (instance.data.events.keyup) {
            AppEvents.run(instance.data.events.keyup, event.target.value);
        }
    }
    //
    // "keydown .Basic": (event, instance) => {
    //     AppEvents.run(instance.data.events.keydown, event.target.value);
    // },
    //
    // "click .Basic": (event, instance) => {
    //     AppEvents.run(instance.data.events.click, event.target.value);
    // },
});

Template.FormsInputBasic.helpers({
    'getColMd': ()=>{
        if (Template.instance().data.options.col_md) return Template.instance().data.options.col_md;
        else return 6;
    }
});
