const initSelect = (query) => {
    $(query).selectpicker();

    $(`form[id^='validate'] ${query}`).each(function() {
        $(this).next('div.bootstrap-select').attr('id', prefix + $(this).attr('id')).removeClass('validate[required]');
    });

    $(query).on('change', function() {
        if ($(this).val() == '' || null === $(this).val()) {
            if (!$(this).attr('multiple')) {
                $(this).val('').find('option').removeAttr('selected').prop('selected', false);
            }
        }

        else {
            $(this).find(`option[value="${$(this).val()}"]`).attr('selected', true);
        }
    });
};

Template.FormsInputSelect.onRendered(function() {
     initSelect('.select');
});

Template.FormsInputSelect.events({
    'change .Select': () => {
        // AppEvents.run(instance.data.events.change, event.target.value);
    },

    'keyup .Select': () => {
        // AppEvents.run(instance.data.events.keyup, event.target.value);
    },

    'keydown .Select': () => {
        // AppEvents.run(instance.data.events.keydown, event.target.value);
    },

    'click .Select': (event) => {
        if (!$(event.currentTarget).hasClass('open'))$(event.currentTarget).addClass('open');
        else $(event.currentTarget).removeClass('open');
        // AppEvents.run(instance.data.events.keydown, event.target.value);
    }
});

Template.FormsInputSelect.helpers({
    'getColMd': ()=>{
        if (Template.instance().data.options.col_md) return Template.instance().data.options.col_md;
        else return 6;
    },

    'refresh': () => {
        let i = 0;
        const interval = setInterval(function() {
            $('.select').selectpicker('refresh');
            i++;
            if (i == 10) {
                clearInterval(interval);
            }
        }, 100);
    }
});
