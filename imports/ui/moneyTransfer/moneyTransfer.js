import {get} from 'lodash';

Template.MoneyTransferComponent.onCreated(function() {
    AppState.set('MoneyTransferComponent.ownAccounts', get(Template.instance().data, `options.inputs[0].options.optionsList[0]`, undefined));
    AppState.set('MoneyTransferComponent.withinBank', get(Template.instance().data, `options.inputs[0].options.optionsList[0]`, undefined));
    AppState.set('MoneyTransferComponent.internationalPayments', get(Template.instance().data, `options.inputs[0].options.optionsList[0]`, undefined));
    AppState.set('AccountsForTransaction', new AppQuery(
        Template.instance().data.options.query
    ));
    AppState.set('from_to_btn', true);
});

Template.MoneyTransferComponent.events({
    'change .Select': () =>{
        AppState.set('MoneyTransferComponent.ownAccounts', $('.Select').val());
        AppState.set('MoneyTransferComponent.withinBank', $('.Select').val());
        AppState.set('MoneyTransferComponent.internationalPayments', $('.Select').val());
    },
    'click .reset': () =>{
        let reject = confirm('Are you sure?');
        if (reject) history.back();
    },
    'click .from_to_btn': () =>{
        AppState.set('from_to_btn', !AppState.get('from_to_btn'));
    },
    'submit .MoneyTransfer': () =>{
        return false;
    }
});

Template.MoneyTransferComponent.helpers({
    'getDB': (index)=>{
        const inputData = get(Template.instance().data, `options.inputs[${index}]`, undefined);
        let documents = AppState.get('AccountsForTransaction');
        let accouunList = [];
        for (let i = 0; i < documents.length; i++) {
            accouunList.push(documents[i].AccountId);
        }
        if ( (index == 0) ||
            (index == 3) ||
            (index == 4) ||
            (index == 6) ||
            (index == 8) ||
            (index == 9) ||
            (index == 10) ||
            (index == 11)) { // USE array some for check up conditions like this
            return {
                ...inputData,
                options: {
                    ...inputData.options,
                    col_md: 9
                }
            };
        } else if (index == 1) {
            return {
                ...inputData,
                options: {
                    ...inputData.options,
                    col_md: 9,
                    optionsList: AppState.get('from_to_btn') ? [AppState.get('CurrentCard').AccountNo] : accouunList
                }
            };
        } else if (index == 2) {
            return {
                ...inputData,
                options: {
                    ...inputData.options,
                    col_md: 9,
                    optionsList: !AppState.get('from_to_btn') ? [AppState.get('CurrentCard').AccountNo] : accouunList
                }
            };
        } else if ((index == 5) ||
                (index == 7) ||
                (index == 12) ||
                (index == 13)) {
            return {
                ...inputData,
                options: {
                    ...inputData.options,
                    col_md: 9,
                    optionsList: [
                        'Please select'
                    ]
                }
            };
        }
    },
    'ownAccounts': ()=>{
        return AppState.get('MoneyTransferComponent.ownAccounts') == get(Template.instance().data, `options.inputs[0].options.optionsList[0]`, undefined) ? true : false;
    },
    'withinBank': ()=>{
        return AppState.get('MoneyTransferComponent.withinBank') == get(Template.instance().data, `options.inputs[0].options.optionsList[1]`, undefined) ? true : false;
    },
    'internationalPayments': ()=>{
        return AppState.get('MoneyTransferComponent.internationalPayments') == get(Template.instance().data, `options.inputs[0].options.optionsList[3]`, undefined) ? true : false;
    }
});
