import * as config from './config';
import {get} from 'lodash';

const setCurrentEvents = (events) => {
    const {
        currentYear,
        currentMonth,
        currentDate
    } = Template.instance();

    const date = new Date(
        currentYear.get(),
        currentMonth.get(),
        currentDate.get()
    );

    if (events === null) {
        events = [];
    }

    AppState.set('Calendar.events.current', events.filter((x) => {
        return datesEqual(x.date, date);
    }));
};

const datesEqual = (date1, date2) => {
    return (
            date1.getDate() == date2.getDate()
        ) && (
            date1.getMonth() == date2.getMonth()
        ) && (
            date1.getYear() == date2.getYear()
        );
};

Template.Calendar.onCreated(function() {
    const currentDate = new Date();

    this.months = config.getMonths();
    this.days = config.days;

    this.currentMonth   = new ReactiveVar(currentDate.getMonth());
    this.currentYear    = new ReactiveVar(currentDate.getFullYear());
    this.currentDay     = new ReactiveVar(currentDate.getDay());
    this.currentDate    = new ReactiveVar(currentDate.getDate());

    this.currentView    = new ReactiveVar('calendar'); // calendar, months, years

    AppState.set('Calendar.eventTypes', {
        Loans: {color: 'red'},
        Deposits: {color: 'black'}
    });
});

Template.Calendar.onRendered(function() {
    if ($('div.cal-today').find('i').hasClass('fa')) $('.pagination.pagination-sm').show();
    else $('.pagination.pagination-sm').hide();
});

Template.Calendar.helpers({
    days: () => Template.instance().days,
    weeks: () => {
        const configMonths  = Template.instance().months;
        const configDays    = Template.instance().days;

        const currentMonth  = Template.instance().currentMonth.get();
        const currentYear   = Template.instance().currentYear.get();
        const currentDay    = Template.instance().currentDay.get();

        console.log(' ????? ', currentMonth);

        const daysCount = configMonths[currentMonth].daysCount;

        const weeks = [];
        const firstWeek = [];
        const lastWeek = [];

        let firstMon = 1;
        let lastSun  = daysCount;

        let firstDay = new Date(currentYear, currentMonth, 1).getDay();
        let lastDay  = new Date(currentYear, currentMonth, daysCount).getDay();

        firstDay = firstDay == 0 ? 7 : firstDay;
        lastDay  = lastDay == 0 ? 7 : lastDay;

        if (firstDay !== 1) {
            const prevMonthNumber = (12 + currentMonth - 1) % 12;
            const prevMonth = configMonths[prevMonthNumber];
            let i = 1;

            for (; i < firstDay; i++) {
                firstWeek.push({
                    month: prevMonthNumber,
                    number: prevMonth.daysCount - firstDay + i + 1,
                    year: prevMonthNumber == 11 ? currentYear - 1 : currentYear
                });
            }

            for (; i <= 7; i++) {
                firstWeek.push({
                    month: currentMonth,
                    number: (i - firstDay) + 1,
                    year: currentYear
                });
            }

            firstMon += 7 - firstDay + 1;
        }

        if (lastDay !== 7) {
            const nextMonthNumber = (currentMonth + 1) % 12;
            const nextMonth = configMonths[nextMonthNumber];

            let i = lastDay - 1;
            for (; i >= 0; i--) {
                lastWeek.push({
                    month: currentMonth,
                    number: daysCount - i,
                    year: currentYear
                });
            }

            for (i = lastDay + 1; i <= 7; i++) {
                lastWeek.push({
                    month: nextMonthNumber,
                    number: i - lastDay,
                    year: nextMonthNumber == 0 ? currentYear + 1 : currentYear
                });
            }

            lastSun = daysCount - lastDay;
        }

        weeks.push(firstWeek);

        const weekCount = Math.ceil((lastSun - firstMon)/7);
        let currentNumber = firstMon;
        let currentWeek = [];

        for (let i = 0; i < weekCount; i++) {
            for (let j = 0; j < 7; currentNumber++, j++) {
                currentWeek.push({
                    month: currentMonth,
                    number: currentNumber,
                    year: currentYear
                });
            }

            weeks.push(currentWeek);
            currentWeek = [];
        }

        weeks.push(lastWeek);

        let overallWeeksCount = weeks.length;
        if (firstWeek.length == 0) {
            overallWeeksCount--;
        }

        if (lastWeek.length == 0) {
            overallWeeksCount--;
        }

        if (overallWeeksCount < 6) {
            let needWeeks = 6 - overallWeeksCount;
            const lastReadyWeek = [...weeks[weeks.length - 1]];
            const lastDisplayedWeek = lastReadyWeek.length == 0 ? [...weeks[weeks.length - 2]] : lastReadyWeek;

            for (let additWeek = 0; additWeek < needWeeks; additWeek++) {
                const lastWeekMonth = lastDisplayedWeek[6].month;
                const lastWeekNumber = lastDisplayedWeek[6].number;

                const anotherWeek = [];

                const nextMonth = lastWeekMonth == currentMonth ? (lastWeekMonth + 1) % 12 : lastWeekMonth;
                const initialNum = lastWeekMonth == currentMonth ? 0 : lastWeekNumber;
                const nextYear = nextMonth == 0 ? lastDisplayedWeek[6].year + 1 : lastDisplayedWeek[6].year;

                for (let i = 1; i <= 7; i++) {
                    const day = {
                        month: nextMonth,
                        number: initialNum + i,
                        year: nextYear
                    };

                    lastDisplayedWeek.shift();
                    anotherWeek.push(day);
                    lastDisplayedWeek.push(day);
                }

                weeks.push(anotherWeek);
            }
        }

        return weeks;
    },
    month: () => {
        const {months, currentMonth} = Template.instance();

        return months[currentMonth.get()];
    },
    year: () => Template.instance().currentYear.get(),

    thisMonth: (day, cssClass, altCssClass) => (
        day.month == Template.instance().currentMonth.get() ? cssClass : altCssClass
    ),
    thisDay: (day, cssClass) => (
            day.number == Template.instance().currentDate.get()
        ) && (
            day.month == Template.instance().currentMonth.get()
        ) && (
            day.year == Template.instance().currentYear.get()
        ) ? cssClass : '',

    today: (day, cssClass) => {
        const today = new Date();

        return (
                day.number == today.getDate()
            ) && (
                day.month == today.getMonth()
            ) && (
                day.year == today.getFullYear()
            ) ? cssClass : '';
    },
    events: () => {
        AppEvents.run('GetCalendarEvents');
        const events = AppState.get('Calendar.events');
        setCurrentEvents(events);
        return AppState.get('Calendar.events.current');
    },

    eventsTypes: (day) => {
        const date = new Date(
            day.year,
            day.month,
            day.number
        );

        const types = {};
        const events = (AppState.get('Calendar.events') || [])
            .filter((x) => datesEqual(x.date, date));

        events.forEach((x) => {
            types[x.type + ''] = true;
        });

        const eventTypes = AppState.get('Calendar.eventTypes');
        const presentTypes = [];

        Object.keys(types).forEach((type) => {
            presentTypes.push({
                type,
                color: eventTypes[type].color
            });
        });

        return presentTypes;
    },

    paginationOptions: () => ({
        options: {
            dataSource: 'Calendar.events.current',
            dataPerPage: 5,
            size: 'sm'
        }
    }),

    selectMonths: (half) => {
        const offset = half == 1 ? 0 : 6;

        return Template.instance().months
            .slice(offset, offset + 6)
            .map((x, i) => ({...x, i: offset + i}));
    },

    localeTime: (time) => time.toLocaleString('en-US', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric'
    }),

    equal: (val1, val2) => val1 == val2
});

Template.Calendar.events({
    'click .cal-prevMon': (e, i) => {
        i.currentMonth.set((12 + i.currentMonth.get() - 1) % 12);
        if (i.currentMonth.get() == 11) {
            i.currentYear.set(i.currentYear.get() - 1);
        }
        i.currentDate.set(1);
    },

    'click .cal-nextMon': (e, i) => {
        i.currentMonth.set((i.currentMonth.get() + 1) % 12);
        if (i.currentMonth.get() == 0) {
            i.currentYear.set(i.currentYear.get() + 1);
        }
        i.currentDate.set(1);
    },

    'click .cal-prevYear': (e, i) => {
        i.currentYear.set(i.currentYear.get() - 1);
    },

    'click .cal-nextYear': (e, i) => {
        i.currentYear.set(i.currentYear.get() + 1);
    },

    'click a.cal-day': (e) => {
        const {month, number, year} = e.currentTarget.dataset;
        const {
            currentMonth,
            currentYear,
            currentDate
        } = Template.instance();

        currentMonth.set(parseInt(month) % 12);
        currentYear.set(parseInt(year));
        currentDate.set(parseInt(number));

        const events = AppState.get('Calendar.events');
        setCurrentEvents(events);

        if ($(e.target).parents(':eq(1)').find('i').hasClass('fa')) $('.pagination.pagination-sm').show();
        else $('.pagination.pagination-sm').hide();
    }
});
