const isLeap = (year) => (
        year % 4 == 0
    ) && (
        year % 100 !== 0
    ) || (
        year % 400 == 0
    );

const months = [
    {name: 'January', daysCount: 31},
    {name: 'February', daysCount: 28},
    {name: 'March', daysCount: 31},
    {name: 'April', daysCount: 30},
    {name: 'May', daysCount: 31},
    {name: 'June', daysCount: 30},
    {name: 'July', daysCount: 31},
    {name: 'August', daysCount: 31},
    {name: 'September', daysCount: 30},
    {name: 'October', daysCount: 31},
    {name: 'November', daysCount: 30},
    {name: 'December', daysCount: 31}
];

export const getMonths = () => {
    const readyMonths = [...months];
    readyMonths[1].daysCount = isLeap((new Date()).getYear()) ? 29 : 28;

    return months;
};

export const days = [
    {full: 'Monday',    short: 'Mon'},
    {full: 'Tuesday',   short: 'Tue'},
    {full: 'Wednesday', short: 'Wed'},
    {full: 'Thursday',  short: 'Thu'},
    {full: 'Friday',    short: 'Fri'},
    {full: 'Saturday',  short: 'Sat'},
    {full: 'Sunday',    short: 'Sun'}
];
