db.settings.remove({_id: "cardsview-staff"});
db.settings.remove({_id: "dataview-staff"});
db.settings.insert({ 
    "_id" : "cardsview-staff", 
    "template" : false, 
    "favourite" : false, 
    "type" : "dataview", 
    "isTable" : false, 
    "content" : {
        "cardWidth" : NumberInt(6), 
        "query" : "Staff.find({});", 
        "system" : {
            "rowsPerPage" : NumberInt(6), 
            "showColumnToggles" : false, 
            "showFilter" : false, 
            "showRowCount" : true, 
            "showNavigation" : "always", 
            "showNavigationRowsPerPage" : true, 
            "noDataTmpl" : "Template.noData", 
            "useFontAwesome" : true, 
            "fields" : [
                {
                    "fieldId" : "firstName", 
                    "key" : "FirstNameEng", 
                    "label" : "First Name", 
                    "hidden" : false
                }, 
                {
                    "fieldId" : "lastName", 
                    "key" : "LastNameEng", 
                    "label" : "Last Name"
                }, 
                {
                    "fieldId" : "branchName", 
                    "key" : "BranchName", 
                    "label" : "Branch", 
                    "sortable" : false
                }, 
                {
                    "fieldId" : "departmentName", 
                    "key" : "DepartmentName", 
                    "label" : "Department", 
                    "sortOrder" : NumberInt(0), 
                    "sortDirection" : "descending"
                }, 
                {
                    "fieldId" : "positionName", 
                    "key" : "PositionName", 
                    "label" : "Position"
                }, 
                {
                    "fieldId" : "birthDate", 
                    "key" : "BirthDate", 
                    "label" : "Birth Date", 
                    "fn" : "object[str].slice(0, -19);"
                }, 
                {
                    "fieldId" : "email", 
                    "key" : "EMail", 
                    "label" : "E-mail", 
                    "fn" : "'<a href=\"mailto:' + object[str] + '\">' + object[str] + '</a>';"
                }
            ]
        }, 
        "filters" : [
            "FirstNameEng", 
            "BranchName", 
            "DepartmentName", 
            "PositionName"
        ], 
        "pagination" : {
            "perPage" : NumberInt(6), 
            "pageCount" : NumberInt(5), 
            "showPages" : true, 
            "showPrevious" : true, 
            "showNext" : true
        }
    }
});
db.settings.insert({ 
    "_id" : "dataview-staff", 
    "template" : false, 
    "favourite" : false, 
    "type" : "dataview", 
    "isTable" : true, 
    "content" : {
        "cardWidth" : NumberInt(6), 
        "query" : "Staff.find({});", 
        "system" : {
            "rowsPerPage" : NumberInt(10), 
            "showColumnToggles" : false, 
            "showFilter" : false, 
            "showRowCount" : true, 
            "showNavigation" : "always", 
            "showNavigationRowsPerPage" : true, 
            "noDataTmpl" : "Template.noData", 
            "useFontAwesome" : true, 
            "fields" : [
                {
                    "fieldId" : "firstName", 
                    "key" : "FirstNameEng", 
                    "label" : "First Name", 
                    "hidden" : false
                }, 
                {
                    "fieldId" : "lastName", 
                    "key" : "LastNameEng", 
                    "label" : "Last Name"
                }, 
                {
                    "fieldId" : "branchName", 
                    "key" : "BranchName", 
                    "label" : "Branch", 
                    "sortable" : false
                }, 
                {
                    "fieldId" : "departmentName", 
                    "key" : "DepartmentName", 
                    "label" : "Department", 
                    "sortOrder" : NumberInt(0), 
                    "sortDirection" : "descending"
                }, 
                {
                    "fieldId" : "positionName", 
                    "key" : "PositionName", 
                    "label" : "Position"
                }, 
                {
                    "fieldId" : "birthDate", 
                    "key" : "BirthDate", 
                    "label" : "Birth Date", 
                    "fn" : "object[str].slice(0, -19);"
                }, 
                {
                    "fieldId" : "email", 
                    "key" : "EMail", 
                    "label" : "E-mail", 
                    "fn" : "'<a href=\"mailto:' + object[str] + '\">' + object[str] + '</a>';"
                }
            ]
        }, 
        "filters" : [
            "FirstNameEng", 
            "BranchName", 
            "DepartmentName", 
            "PositionName"
        ], 
        "pagination" : {
            "perPage" : NumberInt(10), 
            "pageCount" : NumberInt(5), 
            "showPages" : true, 
            "showPrevious" : true, 
            "showNext" : true
        }
    }
});