db.settings.remove({_id: "cardsview-staff"});
db.settings.remove({_id: "dataview-staff"});
db.settings.remove({_id: "dataview-settings"});
db.widgets.remove({_id: "dataview-template"});
db.widgets.insert({ 
    "_id" : "dataview-template", 
    "rank" : NumberInt(0), 
    "type" : "dataview", 
    "settings" : {
        "template" : true, 
        "favourite" : false, 
        "type" : "dataview", 
        "isTable" : true, 
        "content" : {
            "cardWidth" : NumberInt(4), 
            "query" : "Demo.find({});", 
            "system" : {
                "rowsPerPage" : NumberInt(4), 
                "showColumnToggles" : false, 
                "showFilter" : false, 
                "showRowCount" : true, 
                "showNavigation" : "always", 
                "showNavigationRowsPerPage" : true, 
                "noDataTmpl" : "Template.noData", 
                "useFontAwesome" : true
            }, 
            "filters" : [
                "name", 
                "position", 
                "office", 
                "salary"
            ], 
            "fields" : [
                {
                    "fieldId" : "name", 
                    "key" : "name", 
                    "label" : "Name", 
                    "hidden" : false
                }, 
                {
                    "fieldId" : "position", 
                    "key" : "position", 
                    "label" : "Position"
                }, 
                {
                    "fieldId" : "gender", 
                    "key" : "gender", 
                    "label" : "Gender", 
                    "sortable" : false
                }, 
                {
                    "fieldId" : "office", 
                    "key" : "office", 
                    "label" : "Office", 
                    "sortOrder" : NumberInt(0), 
                    "sortDirection" : "descending"
                }, 
                {
                    "fieldId" : "age", 
                    "key" : "age", 
                    "label" : "Age"
                }, 
                {
                    "fieldId" : "startDate", 
                    "key" : "startDate", 
                    "label" : "Start Date"
                }, 
                {
                    "fieldId" : "salary", 
                    "key" : "salary", 
                    "label" : "Salary", 
                    "hidden" : true, 
                    "fn" : "'$' + object[str]"
                }
            ], 
            "pagination" : {
                "perPage" : NumberInt(10), 
                "pageCount" : NumberInt(5), 
                "showPages" : true, 
                "showPrevious" : true, 
                "showNext" : true
            }
        }
    }, 
    "style" : {
        "widthMU" : NumberInt(12)
    }, 
    "template" : true, 
    "new" : false
});
db.settings.insert({ 
    "_id" : "dataview-settings", 
    "template" : false, 
    "favourite" : false, 
    "type" : "dataview", 
    "isTable" : true, 
    "content" : {
        "cardWidth" : NumberInt(4), 
        "query" : "Demo.find({});", 
        "system" : {
            "rowsPerPage" : NumberInt(4), 
            "showColumnToggles" : false, 
            "showFilter" : false, 
            "showRowCount" : true, 
            "showNavigation" : "always", 
            "showNavigationRowsPerPage" : true, 
            "noDataTmpl" : "Template.noData", 
            "useFontAwesome" : true
        }, 
        "filters" : [
            "name", 
            "position", 
            "office", 
            "salary"
        ], 
        "fields" : [
            {
                "fieldId" : "name", 
                "key" : "name", 
                "label" : "Name", 
                "hidden" : false
            }, 
            {
                "fieldId" : "position", 
                "key" : "position", 
                "label" : "Position"
            }, 
            {
                "fieldId" : "gender", 
                "key" : "gender", 
                "label" : "Gender", 
                "sortable" : false
            }, 
            {
                "fieldId" : "office", 
                "key" : "office", 
                "label" : "Office", 
                "sortOrder" : NumberInt(0), 
                "sortDirection" : "descending"
            }, 
            {
                "fieldId" : "age", 
                "key" : "age", 
                "label" : "Age"
            }, 
            {
                "fieldId" : "startDate", 
                "key" : "startDate", 
                "label" : "Start Date"
            }, 
            {
                "fieldId" : "salary", 
                "key" : "salary", 
                "label" : "Salary", 
                "hidden" : true, 
                "fn" : "'$' + object[str]"
            }
        ], 
        "pagination" : {
            "perPage" : NumberInt(10), 
            "pageCount" : NumberInt(5), 
            "showPages" : true, 
            "showPrevious" : true, 
            "showNext" : true
        }
    }
});
db.settings.insert({ 
    "_id" : "dataview-staff", 
    "template" : false, 
    "favourite" : false, 
    "type" : "dataview", 
    "isTable" : true, 
    "content" : {
        "cardWidth" : NumberInt(6), 
        "query" : "Staff.find({});", 
        "system" : {
            "rowsPerPage" : NumberInt(10), 
            "showColumnToggles" : false, 
            "showFilter" : false, 
            "showRowCount" : true, 
            "showNavigation" : "always", 
            "showNavigationRowsPerPage" : true, 
            "noDataTmpl" : "Template.noData", 
            "useFontAwesome" : true
        }, 
        "fields" : [
            {
                "fieldId" : "firstName", 
                "key" : "FirstNameEng", 
                "label" : "First Name", 
                "hidden" : false
            }, 
            {
                "fieldId" : "lastName", 
                "key" : "LastNameEng", 
                "label" : "Last Name"
            }, 
            {
                "fieldId" : "branchName", 
                "key" : "BranchName", 
                "label" : "Branch", 
                "sortable" : false
            }, 
            {
                "fieldId" : "departmentName", 
                "key" : "DepartmentName", 
                "label" : "Department", 
                "sortOrder" : NumberInt(0), 
                "sortDirection" : "descending"
            }, 
            {
                "fieldId" : "positionName", 
                "key" : "PositionName", 
                "label" : "Position"
            }, 
            {
                "fieldId" : "birthDate", 
                "key" : "BirthDate", 
                "label" : "Birth Date", 
                "fn" : "object[str].slice(0, -19);"
            }, 
            {
                "fieldId" : "email", 
                "key" : "EMail", 
                "label" : "E-mail", 
                "fn" : "'<a href=\"mailto:' + object[str] + '\">' + object[str] + '</a>';"
            }
        ], 
        "filters" : [
            "FirstNameEng", 
            "BranchName", 
            "DepartmentName", 
            "PositionName"
        ], 
        "pagination" : {
            "perPage" : NumberInt(10), 
            "pageCount" : NumberInt(5), 
            "showPages" : true, 
            "showPrevious" : true, 
            "showNext" : true
        }
    }
});
db.settings.insert({ 
    "_id" : "cardsview-staff", 
    "template" : false, 
    "favourite" : false, 
    "type" : "dataview", 
    "isTable" : false, 
    "content" : {
        "cardWidth" : NumberInt(6), 
        "query" : "Staff.find({});", 
        "system" : {
            "rowsPerPage" : NumberInt(6), 
            "showColumnToggles" : false, 
            "showFilter" : false, 
            "showRowCount" : true, 
            "showNavigation" : "always", 
            "showNavigationRowsPerPage" : true, 
            "noDataTmpl" : "Template.noData", 
            "useFontAwesome" : true
        }, 
        "fields" : [
            {
                "fieldId" : "firstName", 
                "key" : "FirstNameEng", 
                "label" : "First Name", 
                "hidden" : false
            }, 
            {
                "fieldId" : "lastName", 
                "key" : "LastNameEng", 
                "label" : "Last Name"
            }, 
            {
                "fieldId" : "branchName", 
                "key" : "BranchName", 
                "label" : "Branch", 
                "sortable" : false
            }, 
            {
                "fieldId" : "departmentName", 
                "key" : "DepartmentName", 
                "label" : "Department", 
                "sortOrder" : NumberInt(0), 
                "sortDirection" : "descending"
            }, 
            {
                "fieldId" : "positionName", 
                "key" : "PositionName", 
                "label" : "Position"
            }, 
            {
                "fieldId" : "birthDate", 
                "key" : "BirthDate", 
                "label" : "Birth Date", 
                "fn" : "object[str].slice(0, -19);"
            }, 
            {
                "fieldId" : "email", 
                "key" : "EMail", 
                "label" : "E-mail", 
                "fn" : "'<a href=\"mailto:' + object[str] + '\">' + object[str] + '</a>';"
            }
        ], 
        "filters" : [
            "FirstNameEng", 
            "BranchName", 
            "DepartmentName", 
            "PositionName"
        ], 
        "pagination" : {
            "perPage" : NumberInt(6), 
            "pageCount" : NumberInt(5), 
            "showPages" : true, 
            "showPrevious" : true, 
            "showNext" : true
        }
    }
});
