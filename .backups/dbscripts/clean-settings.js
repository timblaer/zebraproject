var end = [];

function getSettingsArray(obj){
    for (prop in obj) {
      if(typeof obj[prop] == "object") {
      		getSettingsArray(obj[prop])
      } else {
      	if(prop == "settings") {
      		end.push(obj[prop]);
      	}
      } 
	}
}

db.widgets.find({}).forEach(function(widget){
	getSettingsArray(widget);
});

db.conditions.find({}).forEach(function(condition){
	getSettingsArray(condition);
});

db.settings.find({}).forEach(function(setting){
	getSettingsArray(setting);
});

db.menu_items.find({}).forEach(function(item){
	getSettingsArray(item);
});

db.pages.find({}).forEach(function(page){
	getSettingsArray(page);
});

print('BEGIN');
db.settings.find({}).forEach(function(setting){
	if(end.indexOf(setting._id) === -1) {
		print('"' + setting._id + '",');
	}
});
print('THE END');












