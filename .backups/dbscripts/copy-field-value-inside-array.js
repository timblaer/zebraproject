function addEventTsField(){
    db.conditions.find({ "rules.settingId": { $exists: true } }).forEach(function(doc){
      
      	doc.rules.forEach(function(rule, i) {
      	  	 var name =  "rules."+i+".settings";
      	  	 var value = rule.settingId;
      	  	 
      	  	  var set = {};
 			 set[name] = value;
      	  	
      		 db.conditions.update({_id:doc._id}, {$set:set});
      	});
        
    });
}

addEventTsField();