function removeFieldInArray(){
    db.conditions.find({ "rules.settingId": { $exists: true } }).forEach(function(doc){
      
      	doc.rules.forEach(function(rule, i) {
      	  	 var name =  "rules."+i+".settingId";
      	  	 
      	  	  var set = {};
 			 set[name] = "";
      	  	
      		 db.conditions.update({_id:doc._id}, {$unset:set});
      	});
        
    });
}

removeFieldInArray();