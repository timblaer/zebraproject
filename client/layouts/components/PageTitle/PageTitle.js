Template.PageTitle.events({
    'click .history-back': () => {
        let answer = confirm('Do you want to leave this page?');
        if (answer) {
            history.length ? history.back() : null;
        }
    }
});
