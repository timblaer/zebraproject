import Utils from '/imports/libs/Utils';

Template.Navigation.onRendered(function() {
});

Template.Navigation.helpers({
    getUserName: () => {
        const lang = (
            AppState.get('Dictionary.CurrentLanguage') == 'RU'
        ) ? 'EN' : AppState.get('Dictionary.CurrentLanguage');
        return Utils.dataGetter(`user.personalData:Name:FullName:${lang}`, AppStorage);
    }
});

Template.Navigation.events({
    'click .x-navigation .xn-openable > a': function() {
        const target = event.target;
        if ($(target).parent('li').hasClass('active')) {
           $(target).parent('li').removeClass('active');
        } else {
            $(target).parents('li').addClass('active');
        }
    }
});
