Template.NavigationVertical.helpers({
    getCurrentLang: () => {
        const lang = AppState.get('Dictionary.CurrentLanguage');
        switch (lang.toLowerCase()) {
            case 'en':
                return 'gb';

            default:
                return lang.toLowerCase();
        }
    }
});

Template.NavigationVertical.events({
    'click .toggle-dashboard-settings': () => {
        const dashboard = Dashboards.findOne();

        if (AppState.get('Dashboard.edit')) {
            Meteor.call('Dashboard.settings.save', {
                widgets: AppState.get('Dashboard.currentWidgets')
            }, function(err) {
                    if (err)AppLog('err', err);
            });
        }

        AppState.set(
            'Dashboard.edit',
            !AppState.get('Dashboard.edit')
        );

        AppState.set(
            'Dashboard.currentWidgets',
            AppState.get('Dashboard.edit') ? dashboard.widgets : []
        );
    },

    'click .logout': () => {
        $('#mb-signout').addClass('open');
    },
    'click #YesBtn': () => {
         AppStorage.clear();
    },
    'click #NoBtn': () => {
        $('#mb-signout').removeClass('open');
    },


    'click a.lang-switcher': (e) => {
        e.stopPropagation();
        $('li.lang-switcher').toggleClass('active');
    },

    'click .lang-switcher-option': (e) => {
        AppStorage.set('Dictionary.CurrentLanguage', e.target.dataset.lang);
        AppState.set('Dictionary.CurrentLanguage', e.target.dataset.lang);
        $('li.lang-switcher').toggleClass('active');
    }
});
