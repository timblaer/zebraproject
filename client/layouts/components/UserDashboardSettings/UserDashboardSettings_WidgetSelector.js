import {get, isArray} from 'lodash';

Template.UserDashboardSettings_WidgetSelector.helpers({
    availableWidgets: () => {
        return get(Config.findOne('dashboard_widgets'), 'widgets', []);
    },

    isPresent: (widgetName) => {
        const currentWidgets = AppState.get('Dashboard.currentWidgets');

        return isArray(currentWidgets) ? currentWidgets.indexOf(widgetName) > -1 : false;
    }
});

Template.UserDashboardSettings_WidgetSelector.events({
    'click .ts-button': () => {
        if ($('.theme-settings').hasClass('active')) {
            Meteor.call('Dashboard.settings.save', {
                widgets: AppState.get('Dashboard.currentWidgets')
            }, function(err) {
                if (err)AppLog('err', err);
            });

            AppState.set('Dashboard.edit', false);
        }

        $('.theme-settings').toggleClass('active');
    },

    'click .toggle-dashboard-widget': (e) => {
        const currentWidgets = AppState.get('Dashboard.currentWidgets');
        const selectedWidget = e.target.value;
        const widgetIndex    = currentWidgets.findIndex((x) => x == selectedWidget);

        if (widgetIndex > -1) {
            currentWidgets.splice(widgetIndex, 1);
        } else {
            currentWidgets.push(selectedWidget);
        }

        AppState.set('Dashboard.currentWidgets', currentWidgets);
    }
});
