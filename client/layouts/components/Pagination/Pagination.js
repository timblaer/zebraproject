import {get, range} from 'lodash';
import {onload} from '/client/lib/actions';

Template.Pagination.helpers({
    size: () => get(Template.instance(), 'data.options.size', 'lg'), // or 'sm'
    correct: (x) => x + 1,

    sliced: (options) => {
        const {dataSource, dataPerPage} = get(Template.instance(), 'data.options', {});

        const data       = AppState.get(dataSource);
        const pagination = AppState.get(`${dataSource}.pagination`);

        const sliceFrom  = pagination.current - 2 < 0 ? 0 : pagination.current - 2;

        return options.length <= 5 ? options : options.slice(sliceFrom, sliceFrom + 5);
    },

    options: () => {
        const {dataSource, dataPerPage} = get(Template.instance(), 'data.options', {});

        const data = AppState.get(dataSource);

        const pagination = AppState.get(`${dataSource}.pagination`);
        const current    = get(pagination, 'current', 0);

        AppState.set(`${dataSource}.pagination`, {
            current: current,
            dataPerPage: dataPerPage
        });

        return range(0, Math.ceil(get(data, 'length', 0) / dataPerPage));
    },

    active: (index) => {
        const {dataSource} = get(Template.instance(), 'data.options', {});
        const pagination     = AppState.get(`${dataSource}.pagination`);

        return get(pagination, 'current', 0) == index;
    }
});

Template.Pagination.events({
    'click .pagination-index': (e, i) => {
        const {dataSource} = get(Template.instance(), 'data.options', null);
        const pagination     = AppState.get(`${dataSource}.pagination`);
        const newPageIndex   = +event.target.dataset.index;

        AppState.set(`${dataSource}.pagination`, {...pagination, current: newPageIndex});

        onload();
    },

    'click .pagination-pr': (e, i) => {
        const {dataSource} = get(Template.instance(), 'data.options', null);
        const pagination   = AppState.get(`${dataSource}.pagination`);

        const newPageIndex = pagination.current - 1 < 0 ? 0 : pagination.current - 1;
        AppState.set(`${dataSource}.pagination`, {...pagination, current: newPageIndex});

        onload();
    },

    // TODO: DRY!

    'click .pagination-fw': (e, i) => {
        const {dataSource, dataPerPage} = get(Template.instance(), 'data.options', null);

        const data       = AppState.get(dataSource);
        const pagination = AppState.get(`${dataSource}.pagination`);

        const pageCount    = Math.ceil(get(data, 'length', 0) / dataPerPage);
        const newPageIndex = pagination.current + 1 == pageCount ? pageCount - 1 : pagination.current + 1;

        AppState.set(`${dataSource}.pagination`, {...pagination, current: newPageIndex});

        onload();
    },

    'click .pagination-fb': (e, i) => {
        const {dataSource, dataPerPage} = get(Template.instance(), 'data.options', null);

        const data       = AppState.get(dataSource);
        const pagination = AppState.get(`${dataSource}.pagination`);

        const pageCount    = Math.ceil(get(data, 'length', 0) / dataPerPage);
        const newPageIndex = 0;

        AppState.set(`${dataSource}.pagination`, {...pagination, current: newPageIndex});

        onload();
    },

    'click .pagination-ff': (e, i) => {
        const {dataSource, dataPerPage} = get(Template.instance(), 'data.options', null);

        const data       = AppState.get(dataSource);
        const pagination = AppState.get(`${dataSource}.pagination`);

        const pageCount    = Math.ceil(get(data, 'length', 0) / dataPerPage);
        const newPageIndex = pageCount - 1;

        AppState.set(`${dataSource}.pagination`, {...pagination, current: newPageIndex});

        onload();
    }
});
