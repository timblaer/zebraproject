import {layoutLoaded, page_actions, x_navigation_minimize, onresize} from '/client/lib/actions';
import {set_settings} from '/client/lib/settings';

Template.MainLayout.onCreated(function() {
    const self = this;

    self.ready = new ReactiveVar(false);

    AppState.addCallback(() => {
        if (AppState.isReady()) {
            self.ready.set(true);

            layoutLoaded();
        } else {
            self.ready.set(false);
        }
    });
});

Template.MainLayout.onRendered(function() {
    layoutLoaded();

    const theme_settings = {
        st_head_fixed: 1,
        st_sb_fixed: 1,
        st_sb_scroll: 1,
        st_sb_right: 0,
        st_sb_custom: 0,
        st_sb_toggled: 0,
        st_layout_boxed: 0
    };

    set_settings(theme_settings, false);
});

Template.MainLayout.helpers({
    'isReady': () => Template.instance().ready.get()
});

Template.MainLayout.events({
    'click .page-content': () => {
        $('.xn-icon-button').removeClass('active');
    },

    'click .x-features-nav-open': function() {
        $('.x-hnavigation').toggleClass('active');
    },

    'click .x-features .x-features-profile': function() {
        e.stopPropagation();
        $(this).toggleClass('active');
    },

    'click .x-features .x-features-search': function() {
        e.stopPropagation();
        $(this).addClass('active');
        $(this).find('input[type=text]').focus();
    },

    'click .x-navigation-horizontal .panel': function() {
        e.stopPropagation();
    },

    /* Gallery Items */
    'click .gallery-item .iCheck-helper': function() {
        let wr = $(this).parent('div');
        if (wr.hasClass('checked')) {
            $(this).parents('.gallery-item').addClass('active');
        } else {
            $(this).parents('.gallery-item').removeClass('active');
        }
    },
    'click .gallery-item-remove': function() {
        $(this).parents('.gallery-item').fadeOut(400, function() {
            $(this).remove();
        });
        return false;
    },
    'click #gallery-toggle-items': function() {
        $('.gallery-item').each(function() {
            let wr = $(this).find('.iCheck-helper').parent('div');

            if (wr.hasClass('checked')) {
                $(this).removeClass('active');
                wr.removeClass('checked');
                wr.find('input').prop('checked', false);
            } else {
                $(this).addClass('active');
                wr.addClass('checked');
                wr.find('input').prop('checked', true);
            }
        });
    },
    /* END Gallery Items */

    /* DROPDOWN TOGGLE */
    'click .dropdown-toggle': function() {
        // onresize();
    },
    /* DROPDOWN TOGGLE */

    /* MESSAGE BOX */
    'click .mb-control': function() {
        let box = $($(this).data('box'));
        if (box.length > 0) {
            box.toggleClass('open');

            let sound = box.data('sound');

            if (sound === 'alert') {
                playAudio('alert');
            }

            if (sound === 'fail') {
                playAudio('fail');
            }
        }
        return false;
    },
    'click .mb-control-close': function() {
       $(this).parents('.message-box').removeClass('open');
       return false;
   },
    /* END MESSAGE BOX */

    /* CONTENT FRAME */
    'click .content-frame-left-toggle': function() {
        $('.content-frame-left').is(':visible')
        ? $('.content-frame-left').hide()
        : $('.content-frame-left').show();
        page_content_;// onresize();
    },
    'click .content-frame-right-toggle': function() {
        $('.content-frame-right').is(':visible')
        ? $('.content-frame-right').hide()
        : $('.content-frame-right').show();
        page_content_;// onresize();
    },
    /* END CONTENT FRAME */

    /* MAILBOX */
    'click .mail .mail-star': function() {
        $(this).toggleClass('starred');
    },

    'click .mail-checkall .iCheck-helper': function() {
        let prop = $(this).prev('input').prop('checked');

        $('.mail .mail-item').each(function() {
            let cl = $(this).find('.mail-checkbox > div');
            cl.toggleClass('checked', prop).find('input').prop('checked', prop);
        });
    },
    /* END MAILBOX */

    /* PANELS */

    'click .panel-fullscreen': function() {
        panel_fullscreen($(this).parents('.panel'));
        return false;
    },

    'click .panel-collapse': function() {
        panel_collapse($(this).parents('.panel'));
        $(this).parents('.dropdown').removeClass('open');
        return false;
    },
    'click .panel-remove': function() {
        panel_remove($(this).parents('.panel'));
        $(this).parents('.dropdown').removeClass('open');
        return false;
    },
    'click .panel-refresh': function() {
        let panel = $(this).parents('.panel');
        panel_refresh(panel);

        setTimeout(function() {
            panel_refresh(panel);
        }, 3000);

        $(this).parents('.dropdown').removeClass('open');
        return false;
    },
    /* EOF PANELS */

    /* ACCORDION */
    'click .accordion .panel-title a': function() {
        let blockOpen = $(this).attr('href');
        let accordion = $(this).parents('.accordion');
        let noCollapse = accordion.hasClass('accordion-dc');


        if ($(blockOpen).length > 0) {
            if ($(blockOpen).hasClass('panel-body-open')) {
                $(blockOpen).slideUp(200, function() {
                    $(this).removeClass('panel-body-open');
                });
            } else {
                $(blockOpen).slideDown(200, function() {
                    $(this).addClass('panel-body-open');
                });
            }

            if (!noCollapse) {
                accordion.find('.panel-body-open').not(blockOpen).slideUp(200, function() {
                    $(this).removeClass('panel-body-open');
                });
            }

            return false;
        }
    },
    /* EOF ACCORDION */

    /* DATATABLES/CONTENT HEIGHT FIX */
    'change .dataTables_length select': function() {
        // onresize();
    },
    /* END DATATABLES/CONTENT HEIGHT FIX */

    /* TOGGLE FUNCTION */
    'click .toggle': function() {
        let elm = $('#'+$(this).data('toggle'));
        if (elm.is(':visible')) {
            elm.addClass('hidden').removeClass('show');
        } else {
            elm.addClass('show').removeClass('hidden');
        }

        return false;
    },
    /* END TOGGLE FUNCTION */

    /* LOCK SCREEN */
    'click .lockscreen-box .lsb-access': function() {
        $(this).parent('.lockscreen-box').addClass('active').find('input').focus();
        return false;
    },
    'click .lockscreen-box .user_signin': function() {
        $('.sign-in').show();
        $(this).remove();
        $('.user').hide().find('img').attr('src', 'assets/images/users/no-image.jpg');
        $('.user').show();
        return false;
    },
    /* END LOCK SCREEN */

    /* SIDEBAR */
    'click .sidebar-toggle': function() {
        $('body').toggleClass('sidebar-opened');
        return false;
    },
    'click .sidebar .sidebar-tab': function() {
        $('.sidebar .sidebar-tab').removeClass('active');
        $('.sidebar .sidebar-tab-content').removeClass('active');

        $($(this).attr('href')).addClass('active');
        $(this).addClass('active');

        return false;
    },
    'click .page-container': function() {
       $('body').removeClass('sidebar-opened');
   },
    /* END SIDEBAR */

    /* PAGE TABBED */
    'click .page-tabs a': function() {
        $('.page-tabs a').removeClass('active');
        $(this).addClass('active');
        $('.page-tabs-item').removeClass('active');
        $($(this).attr('href')).addClass('active');
        return false;
    },
    /* END PAGE TABBED */

    /* PAGE MODE TOGGLE */
    'click .page-mode-toggle': function() {
        page_mode_boxed();
        return false;
    },
    /* END PAGE MODE TOGGLE */

    'click .x-navigation-control': function(e) {
        $(e.target).parents('.x-navigation').toggleClass('x-navigation-open');
        return false;
    },

    'click .x-navigation-minimize': function() {
        if ($('.page-sidebar .x-navigation').hasClass('x-navigation-minimized')) {
            $('.page-container').removeClass('page-navigation-toggled');
            x_navigation_minimize('open');
        } else {
            $('.page-container').addClass('page-navigation-toggled');
            x_navigation_minimize('close');
        }

        onresize();

        return false;
    },

    'click .x-navigation  li > a': function() {
        let li = $(this).parent('li');
        let ul = li.parent('ul');
    },

    /* XN-SEARCH */
    'click .xn-search': function() {
        $(this).find('input').focus();
    }
});
