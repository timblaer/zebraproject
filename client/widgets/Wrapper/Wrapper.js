import WrapperHelpers from '/imports/libs/WrapperHelpers';

Template.Wrapper.helpers(WrapperHelpers);

Template.Wrapper.onRendered(function() {
    page_content_onresize();
});
