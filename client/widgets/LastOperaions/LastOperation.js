Template.LastOperations.onRendered(function() {
        $('.datepickerfrom').datepicker({format: 'yyyy-mm-dd'});
        $('.datepickerto').datepicker({format: 'yyyy-mm-dd'});
        let date = new Date();
        $('.datepickerto').val(date.toISOString().slice(0, 10));
        AppState.set('LastTransactionsByPeriodDateFrom', date);

        date = new Date(Date.now() - 6.048e+8);
        $('.datepickerfrom').val(date.toISOString().slice(0, 10));
        AppState.set('LastTransactionsByPeriodDateTo', date);
});


Template.LastOperations.events({
    'change .Datepicker': (event, instance) => {
        let from = $('.datepickerfrom').val();
        let to = $('.datepickerto').val();
        AppState.set('LastTransactionsByPeriodDateFrom', from);
        AppState.set('LastTransactionsByPeriodDateTo', to);
    },
    'keyup .Datepicker': (event, instance) => {
        let from = $('.datepickerfrom').val();
        let to = $('.datepickerto').val();
        AppState.set('LastTransactionsByPeriodDateFrom', from);
        AppState.set('LastTransactionsByPeriodDateTo', to);
    },

    'keydown .Datepicker': (event, instance) => {
        let from = $('.datepickerfrom').val();
        let to = $('.datepickerto').val();
        AppState.set('LastTransactionsByPeriodDateFrom', from);
        AppState.set('LastTransactionsByPeriodDateTo', to);
    },

    'click .Datepicker': (event, instance) => {
        let from = $('.datepickerfrom').val();
        let to = $('.datepickerto').val();
        AppState.set('LastTransactionsByPeriodDateFrom', from);
        AppState.set('LastTransactionsByPeriodDateTo', to);
    }
});
