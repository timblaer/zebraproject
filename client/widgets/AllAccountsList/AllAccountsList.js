import {get} from 'lodash';

Template.AllAccountsList.helpers({
    getComponent: (component) => get(Template.instance().data, `settings.components.${component}`, {})
});
