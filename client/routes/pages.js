Router.configure({
  loadingTemplate: 'Loading',
  notFoundTemplate: 'Error404'
  // layoutTemplate: 'MainLayout'
});

Router.route('/dashboard', {
    name: 'dashboard',

    action() {
        this.layout('MainLayout');

        this.render('Dashboard', {
            to: 'currentContainer'
        });
    }
});

Router.route('/loans', {
    name: 'loans',

    action() {
        this.layout('MainLayout');

        this.render('Loans', {
            to: 'currentContainer'
        });
    }
});

Router.route('/accounts', {
    name: 'accounts',

    action() {
        this.layout('MainLayout');

        this.render('Accounts', {
            to: 'currentContainer'
        });
    },

    waitOn: () => [
        Meteor.subscribe('Widgets'),
        Meteor.subscribe('Config')
    ]
});

Router.route('/accounts/deposits', {
    name: 'accounts_deposits',

    action() {
        this.layout('MainLayout');

        this.render('AccountsDeposits', {
            to: 'currentContainer'
        });
    }
});

Router.route('/accounts/current', {
    name: 'accounts_current',

    action() {
        this.layout('MainLayout');

        this.render('AccountsCurrentAccounts', {
            to: 'currentContainer'
        });
    }
});

Router.route('/accounts/statement', {
    name: 'accounts_statement',

    action() {
        this.layout('MainLayout');

        if (AppState.get('CurrentStatement') === null) {
            Router.go('/accounts/current');
        } else {
            this.render('AccountsStatement', {
                to: 'currentContainer'
            });
        }
    }
});


Router.route('/cards', {
    name: 'cards',

    action() {
        this.layout('MainLayout');

        this.render('Cards', {
            to: 'currentContainer'
        });
    }
});


Router.route('/loans/repayment', {
    name: 'loans_repayment',

    action() {
        this.layout('MainLayout');
        if (AppState.get('CurrentLoan') === null) {
            Router.go('/loans');
        } else {
            this.render('LoansRepaymentContainers', {
                to: 'currentContainer'
            });
        }
    }
});

Router.route('/loans/payment', {
    name: 'loans_payment',

    action() {
        this.layout('MainLayout');
        if (AppState.get('CurrentLoan') === null) {
            Router.go('/loans');
        } else {
            this.render('LoansPayment', {
                to: 'currentContainer'
            });
        }
    }
});

Router.route('accounts/deposits/statement', {
    name: 'deposits_statement',

    action() {
        this.layout('MainLayout');
        if (AppState.get('CurrentDeposit') === null) {
            Router.go('/accounts/deposits');
        } else {
            this.render('DepositCurrent', {
                to: 'currentContainer'
            });
        }
    }
});

Router.route('accounts/deposits/transfer', {
    name: 'accounts_deposits_transfer',

    action() {
        this.layout('MainLayout');
        if (AppState.get('CurrentDeposit') === null) {
            Router.go('/accounts/deposits');
        } else {
            this.render('AccountsDepositsTransfer', {
                to: 'currentContainer'
            });
        }
    }
});

Router.route('cards/moneytransfer', {
    name: 'cards_transfer',

    action() {
        this.layout('MainLayout');
        if (AppState.get('CurrentCard') === null) {
            Router.go('/cards');
        } else {
            this.render('AccountsCardsTransfer', {
                to: 'currentContainer'
            });
        }
    }
});
