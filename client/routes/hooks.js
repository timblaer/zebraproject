import {layoutLoaded} from '/client/lib/actions';

Router.onBeforeAction(function() {
    if (AppStorage.get('user.sessionID') === null && process.env.NODE_ENV == 'production' ) {
        this.redirect('/login');
    }

    if (AppStorage.get('user.sessionID') && !AppStorage.get('user.otp')) {
        this.redirect('/otp');
    }

    const interval = setInterval((function() {
        let i = 0;
        return function() {
            AppState.set('isLoading', true);
            if (i < 20) {
                layoutLoaded();
                if (ProductList.find().fetch().length !== 0) i = 19;
                i++;
            } else {
                AppState.set('isLoading', false);
                clearInterval(interval);
            }
        };
    })(), 500);
    this.next();
}, {
    except: [
        'login',
        'otp',
        'changePassword',
        'forgetPassword'
    ]
});
