Router.configure({
    'notFoundTemplate': 'notFound'
});

Router.route('/', function() {
    this.redirect('dashboard');
});

Router.route('/DataTable', function() {
        this.layout('BlankLayout');
        this.render('DataTable');
});

Router.route('/logout', {
    name: 'logout',
    action: function() {
        AppStorage.clear();
        Router.go('login');
    }
});

Router.route('/login', {
    name: 'login',
    action: function() {
        this.layout('BlankLayout');
        this.render('loginPage');
    }
});

Router.route('/otp', {
    name: 'otp',
    action: function() {
        this.layout('BlankLayout');
        this.render('OtpPage');
    }
});

Router.route('/resetPassword', {
    name: 'forgetPassword', // TODO: FORG_O_T
    action: function() {
        this.layout('BlankLayout');
        this.render('resetPassword');
    }
});

Router.route('/changePassword', {
    name: 'changePassword',
    action: function() {
        this.layout('BlankLayout');
        this.render('changePassword');
    }
});

Router.route('/calendar',
 function() {
        this.layout('BlankLayout');
        this.render('Calendar');
    }
);

Router.route('/ResponsiveTable', {
    waitOn: function() {
        return Meteor.subscribe('ProductList');
    },

    action: function() {
        this.layout('BlankLayout');
        this.render('ResponsiveTable');
    }
});

Router.route('/SimpleTable', {
    waitOn: function() {
        return Meteor.subscribe('ProductList');
    },

    action: function() {
        this.layout('BlankLayout');
        this.render('SimpleTable');
    }
});

Router.route('/Sliders', {
    waitOn: function() {
        return Meteor.subscribe('ProductList');
    },

    action: function() {
        this.layout('BlankLayout');
        this.render('sliders');
    }
});

Router.route('/Messages', {
    waitOn: function() {
        return Meteor.subscribe('ProductList');
    },

    action: function() {
        this.layout('BlankLayout');
        this.render('Messages');
    }
});

Router.route('/MessageBox', {
    waitOn: function() {
        return Meteor.subscribe('ProductList');
    },

    action: function() {
        this.layout('BlankLayout');
        this.render('MessageBox');
    }
});

Router.route('/Forms', function() {
    this.layout('BlankLayout');
    this.render('Forms');
});

Router.route('/Charts', function() {
    this.layout('BlankLayout');
    this.render('Charts');
});

Router.route('/exchange', function() {
    this.layout('BlankLayout');
    this.render('ExchangeRateTest');
});


Router.route('/profile', {
    name: 'profile',

    action() {
        this.layout('MainLayout');

        this.render('ProfileContainer', {
            to: 'currentContainer'
        });
    },

    waitOn: () => [
        Meteor.subscribe('Widgets'),
        Meteor.subscribe('Config')
    ]
});
