import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';
import {get} from 'lodash';

import '/imports/ui/blankComponent/blankComponent.html';

Meteor.startup(function() {
    Meteor.subscribe('Widgets');
    Meteor.subscribe('Config');
    Meteor.subscribe('Dashboards');
    Meteor.subscribe('Dictionary');

    Meteor.subscribe('ProductList');
    Meteor.subscribe('GeneralData');

    const dictionary = get(Dictionary.findOne(), 'dictionary', []);

    if (AppStorage.get('Dictionary.CurrentLanguage') === null) {
        AppStorage.set('Dictionary.CurrentLanguage', 'EN');
    }

    AppState.set('Dictionary.CurrentLanguage', AppStorage.get('Dictionary.CurrentLanguage'));

    AppState.set('Dictionary', new AppQuery({
        'collection': 'Dictionary',
        'method': 'findOne',
        'body': {},
        'props': 'dictionary'
    }));

    AppState.set('ExchangeRate', new AppQuery({
        'collection': 'GeneralData',
        'method': 'findOne',
        'body': {},
        'props': 'ExchangeRate.AZN'
    }));
});

Meteor.startup(function() {
    reCAPTCHA.config({
        publickey: '6LdJTDoUAAAAAJtXssPfJ_xEL3W7rSn7D_nY6F8-',
        hl: 'en' // optional display language
    });
});
