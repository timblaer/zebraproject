import {isFunction} from 'lodash';

class Events {
    constructor() {
        this.events = {};
    }

    set(object) {
        this.events = {
            ...this.events,
            ...object
        };
    }

    run(eventName, ...args) {
        return isFunction(this.events[eventName]) ? this.events[eventName](...args) : [...args];
    }
}

AppEvents = new Events();
