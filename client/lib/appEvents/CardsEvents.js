import {get} from 'lodash';
AppEvents.set({
    'updateCard': function(options) {
        let data = AppState.get(options.query.props);
        if (AppState.get('active.row').length > 3) {
            AppState.set('CurrentCard', AppEvents.run(options.dataProcess, data, AppState.get('active.row')));
        } else {
            AppState.set('CurrentCard', null);
        }
    }
});

AppEvents.set({
    'GetCardInfo': function(Cards, CardNo) {
        let newDoc=[];
        let pos={};
        let i;

        for (i = 0; i < Cards.length; i++) {
            if (Cards[i].CardNo == CardNo) break;
        }

        pos['AccountNo']= Cards[i].AccountNo;
        pos['TotalBalance'] = '312 AZN';
        pos['BlockedAmount'] = '37 AZN';
        pos['AvailableAmount'] = '275 AZN';
        newDoc= pos;
        return newDoc;
    }
});

AppEvents.set({
    'Cards': function(Cards =[]) {
        if (Cards) {
            AppState.set('TotalInfoMode', get(Cards[0], 'CardNo', ''));
        for (let i = 0; i < Cards.length; i++) {
            if (Cards[i].CardNo[0] == 4) Cards[i].Type = '<span><i class=\'fa fa-cc-visa fa-lg\' aria-hidden=\'true\'></i></span>';
        }
        return Cards;
    }
    return [];
    }
});
