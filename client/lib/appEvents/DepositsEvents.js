import {get} from 'lodash';
AppEvents.set({
    'updateDeposit': function(options) {
        let data = AppState.get(options.query.props);
        if (AppState.get('active.row').length > 1) {
            AppState.set('CurrentDeposit', AppEvents.run(options.dataProcess, data, AppState.get('active.row')));
        } else {
            AppState.set('CurrentDeposit', null);
        }
    }
});


AppEvents.set({
    'CurrentDeposit': function() {
        let newDoc = [];
        if (AppState.get('CurrentDeposit')) {
            newDoc = AppState.get('CurrentDeposit').Date;

            let options = {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric'
            };

            for (let i = 0; i < newDoc.length; i++) {
                let time = newDoc[i].PlannedDate.toLocaleString('en-US', options);
                time = time.slice(0, 10);
                newDoc[i].PlannedDate = time;
            }
        }
        return newDoc;
        // return Accounts;
    }
});

AppEvents.set({
    'GetDepositInfo': function(Deposits, ContrID) {
        let newDoc=[];
        let pos={};
        let date = new Date();
        let options = {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric'
        };
        if (Deposits) {
        for (var i = 0; i < Deposits.length; i++) {
            if (Deposits[i].ContrID == ContrID) break;
        }
        if (Deposits[i].PaymentPlan) {
            for (let j = 0; j < Deposits[i].PaymentPlan.length; j++) {
                pos['Date'] = Deposits[i].PaymentPlan;
                 if (new Date(Deposits[i].PaymentPlan[j].PlannedDate) > date) {
                     pos['PlannedDate'] = Deposits[i].PaymentPlan[j].PlannedDate.toLocaleString('en-US', options);
                     pos['PlannedDate'] = pos['PlannedDate'].slice(0, 10);
                     pos['PlannedAmount'] = Deposits[i].PaymentPlan[j].PlannedAmount;
                     break;
                 }
            }
        } else {
            pos['PlannedDate'] = '';
            pos['PlannedAmount'] = '';
        }
        pos['ContrID'] = Deposits[i].ContrID;
        pos['Currency'] = Deposits[i].Currency;
        pos['ProdDescr'] = Deposits[i].ProdDescr;
        if (pos['ProdDescr'] == 'Term Deposit Monthly Private')pos['operations']=false;
        else pos['operations']=true;
        pos['Balance'] = Deposits[i].Balance;
        newDoc = pos;
        return newDoc;
    }
    return [];
    }
});

AppEvents.set({
    'GetTotalDeposits': function(value, Deposits) {
            let TotalInfo=[];
            TotalInfo.push({title: 'AZN', value: 0});
            TotalInfo.push({title: 'USD', value: 0});
            TotalInfo.push({title: 'EUR', value: 0});
            TotalInfo.push({title: 'GBP', value: 0});
            if (Deposits) {
                for (let i = 0; i < Deposits.length; i++) {
                    for (let j = 0; j < TotalInfo.length; j++) {
                        if (TotalInfo[j].title == Deposits[i].Currency) {
                            TotalInfo[j].value += parseFloat(Deposits[i].Balance);
                        }
                    }
                }
            }
            for (let i = 0; i < TotalInfo.length; i++) {
                TotalInfo[i].value = TotalInfo[i].value.toFixed(2);
            }
            return TotalInfo;
    }
});
