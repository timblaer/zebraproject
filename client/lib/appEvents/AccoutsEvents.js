AppEvents.set({
    'updateStatement': function(options) {
        let data = AppState.get(options.query.props);
        if (AppState.get('active.row').length > 3) {
            AppState.set(
                'CurrentStatement',
                AppEvents.run(
                    options.dataProcess,
                    data,
                    AppState.get('active.row')
                )
            );
        } else {
            AppState.set('CurrentStatement', null);
        }
    }
});

AppEvents.set({
    'GetAccountInfo': function(Accounts, AccountId) {
        let now = new Date();

        for (let i = 0; i < Accounts.length; i++) {
            if (Accounts[i].AccountId == AccountId) {
                for (let j = 0; j < Accounts[i].Statement.length; j++) {
                    if (now > new Date(Accounts[i].Statement[j].BookingDate)) {
                        let time = Accounts[i].Statement[j].BookingDate.slice(0, 10);
                        Accounts[i].LastTranDate = time;
                    }
                }
                if (!Accounts[i].LastTranDate) {
                    Accounts[i].LastTranDate = '-';
                }

                return Accounts[i];
            }
        }
    }
});

AppEvents.set({
    'GetTotalBalance': function(money, Accounts) {
        let TotalBalance = [];
        if (money != null && Accounts != null) {
            for (let i = 0; i < money.length; i++) {
            TotalBalance.push({title: money[i].currency, value: 0});
        }
        for (let i = 0; i < Accounts.length; i++) {
            TotalBalance.find(function(element, index, array) {
                if (element.title == Accounts[i].Currency) array[index].value += parseFloat((Accounts[i].Balance * 1).toFixed(2));
                array[index].value = parseFloat((array[index].value).toFixed(2));
            });
        }
        return TotalBalance;
}
    }
});


AppEvents.set({
    'CurrentStatement': function() {
        let newDoc = [];
        if (AppState.get('CurrentStatement')) {
            newDoc = AppState.get('CurrentStatement').Statement;
            for (let i = 0; i < newDoc.length; i++) {
                let time = newDoc[i].BookingDate.slice(0, 10);
                newDoc[i].ProcessingDate = time;
            }
            newDoc.sort(function(a, b) {
                return (b.ProcessingDate - a.ProcessingDate);
            });
        }
        return newDoc;
    }
});

AppEvents.set({
    'Statement': function(Accounts) {
        let newDoc = [];
        Accounts = Accounts ? Accounts : [];
        let options = {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric'
        };

        for (let i = 0; i < Accounts.length; i++) {
            for (let j = 0; j < Accounts[i].Statement.length; j++) {
                let pos ={};
                pos = Accounts[i].Statement[j];
                pos['AccountNumber'] = Accounts[i].AccountId;
                pos['CurrencyId'] = Accounts[i].Currency;
                if (Accounts[i].Statement[j].BookingDate) {
                    let time = Accounts[i].Statement[j].BookingDate.slice(0, 10);
                    Accounts[i].Statement[j].ProcessingDate = time;
                }
                newDoc.push(pos);
            }
        }
        newDoc.sort(function(a, b) {
            return (b.ProcessingDate - a.ProcessingDate);
        });

        return newDoc;
    }
});
