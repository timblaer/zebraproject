AppEvents.set({
    'ArraySearch': function(expression = '', array = []) {
        if (!expression)expression = '';
        if (expression.length >= 1) {
            expression = new RegExp(expression, 'i');
            return array.filter((element) => {
                for (let value in element) {
                    if (expression.test(element[value])) return true;
                }
                return false;
            });
        } else return array;
    },
    'searchWord': function(expression = '') {
          AppState.set('search', expression);
    }
});
