import {get} from 'lodash';
AppEvents.set({
    'updateLoan': function(options) {
        let data = AppState.get(options.query.props);
        if (AppState.get('active.row').length > 3) {
            AppState.set('CurrentLoan', AppEvents.run(options.dataProcess, data, AppState.get('active.row')));
        } else {
            AppState.set('CurrentLoan', null);
        }
    }
});

AppEvents.set({
    'GetTotalLoans': function(value, Loans) {
        let TotalInfo=[];
        TotalInfo.push({title: 'TotalDebt', value: 0});
        if (Loans) {
            for (let i = 0; i < Loans.length; i++) {
                for (let j = 0; j < Loans[i].PaymentPlan.length; j++) {
                    TotalInfo[0].value += parseFloat(Loans[i].PaymentPlan[j].TotalPaid);
                }
            }
        }
        TotalInfo[0].value = TotalInfo[0].value.toFixed(2);
        return TotalInfo;
    }
});

AppEvents.set({
    'Loans': function(Loans = []) {
        let newDoc = [];
        let now = new Date();
        let options = {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric'
        };
        for (let i = 0; i < Loans.length; i++) {
            let pos = {};
            for (var j =  Loans[i].PaymentPlan.length-1; j >= 0; j--) {
                if (Loans[i].PaymentPlan[j].PlanRepayDate < now) break;
            }
                if (j < Loans[i].PaymentPlan.length-1) {
                    AppState.set('TotalInfoMode', get(Loans[i], 'ContractNumber', ''));
                    pos['ContractNumber'] = Loans[i].ContractNumber;
                    pos['ApprovedAmount'] = Loans[i].ApprovedAmount;
                    pos['paymentAmount'] = Loans[i].PaymentPlan[j].TotalUnpaid;
                    let time = Loans[i].PaymentPlan[j+1].PlanRepayDate.toLocaleString('en-US', options);
                    time = time.replace('/', '-');
                    time = time.replace('/', '-');
                    pos['nextPayment'] =  time;
        }
        if (pos.ContractNumber)newDoc.push(pos);
        }

        return newDoc;
}
});


AppEvents.set({
    'GetLoanInfo': function(Loans, ContractNumber) {
        Loans = Loans ? Loans:[];
        let options = {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric'
        };
        let newDoc = [];
        let now = new Date();
        let pos = {};
        let i = 0;
        for (; i < Loans.length; i++) {
            if (Loans[i].ContractNumber == ContractNumber) break;
        }


            for (let k = 0; k < Loans[i].PaymentPlan.length; k++) {
                if (Loans[i].PaymentPlan[k].PlanRepayDate > now) {
                    pos['PrincipalAmount'] = Loans[i].PaymentPlan[k].PrinPlan;
                    pos['InterestAmount'] = Loans[i].PaymentPlan[k].IntPlan;
                    pos['PenaltyAmount'] = Loans[i].PaymentPlan[k].PenPlan;
                    pos['PaymentAmount'] = Loans[i].PaymentPlan[k].TotalUnpaid;
                    pos['Currency'] = Loans[i].CurrencyId;
                    pos['ContractNumber'] = Loans[i].ContractNumber;
                    let TotalDebt = 0;
                    for (let j = k; j < Loans[i].PaymentPlan.length; j++) {
                        TotalDebt += parseFloat(Loans[i].PaymentPlan[j].TotalUnpaid);
                    }
                    pos['TotalDebt'] = TotalDebt.toFixed(2);
                    break;
                }
            }
            pos['PaymentPlan']=Loans[i].PaymentPlan;
        newDoc = pos;
        return newDoc;
    }
});

AppEvents.set({
    'CurrentLoan': function(Loan) {
        let newDoc = [];
        if (AppState.get('CurrentLoan')) {
            newDoc = AppState.get('CurrentLoan').PaymentPlan;
            let options = {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric'
            };
            let now = new Date();
            var pos = [];
            for (let i = 0; i < newDoc.length; i++) {
                if (newDoc[i].PlanRepayDate > now) {
                let time = newDoc[i].PlanRepayDate.toLocaleString('en-US', options);
                time = time.replace('/', '-');
                newDoc[i].PlanRepayDate = time.replace('/', '-');
                time = newDoc[i].FactRepayDate.toLocaleString('en-US', options);
                time = time.replace('/', '-');
                newDoc[i].FactRepayDate = time.replace('/', '-');
                pos.push(newDoc[i]);
            }
            }
        }
        newDoc = pos;
        return newDoc;
    }
});
