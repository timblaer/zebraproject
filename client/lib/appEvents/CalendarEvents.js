AppEvents.set({
    'GetCalendarEvents': function() {
        let DB = ProductList.find({_id: 'E976A3EFD95B31F804C6F18DDBBD1F6F-NEW'}).fetch();
        DB = DB[0];
        let options = {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric'
        };
        let Deposits = DB && DB.Deposits || [];
        let Loans = DB && DB.Loans || [];
        let pos = [];
        let now = new Date();
        for (var i = 0; i < Loans.length; i++) {
            for (var j = Loans[i].PaymentPlan.length-1; j >= 0; j--) {
                if (Loans[i].PaymentPlan[j].PlanRepayDate < now) break;
            }

            while (j < Loans[i].PaymentPlan.length-1 ) {
                var time = Loans[i].PaymentPlan[j+1].PlanRepayDate;
                pos.push({'date': time, 'type': 'Loans', 'title': `Loans (${Loans[i].ContractNumber})`});
                j++;
            }
        }
        for (var i = 0; i < Deposits.length; i++) {
            if (Deposits[i].PaymentPlan) {
                for (var j = Deposits[i].PaymentPlan.length-1; j >= 0; j--) {
                    if (Deposits[i].PaymentPlan[j].PlannedDate) {
                        if (new Date(Deposits[i].PaymentPlan[j].PlannedDate) < now) break;
                    }
                }

                if (j < Deposits[i].PaymentPlan.length-1) {
                    var time = new Date(Deposits[i].PaymentPlan[j+1].PlannedDate);
                    pos.push({'date': time, 'type': 'Deposits', 'title': `Deposits (${Deposits[i].ContrID})`});
                    j++;
                }
            }
        }
        AppState.set('Calendar.events', pos);
    }

});
