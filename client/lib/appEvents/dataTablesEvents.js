import {get} from 'lodash';


AppEvents.set({
    'AccountFilterCA': function(Accounts) {
            Accounts = Accounts ? Accounts : [];
            return Accounts.filter(function(a) {
                    return a.AccProduct == 'CA';
            });
        }
});

AppEvents.set({
    'AccountFilterDP': function(Accounts) {
            Accounts = Accounts ? Accounts : [];
            return Accounts.filter(function(a) {
                    return a.AccProduct == 'DR.CARD';
            });
        }
});
