AppEvents.set({
    'LastTransactions': function(Accounts) {
        let newDoc = [];
        Accounts = Accounts ? Accounts : [];
        for (let i = 0; i < Accounts.length; i++) {
            for (let j = 0; j < Accounts[i].Statement.length; j++) {
                let pos ={};
                pos = Accounts[i].Statement[j];
                pos['AccountNumber'] = Accounts[i].AccountId;
                pos['Amount'] = pos['Balance'] + ' ' + Accounts[i].Currency;
                if (Accounts[i].Statement[j].BookingDate) {
                let time = Accounts[i].Statement[j].BookingDate.slice(0, 10);

                    pos['Date'] = time;
                }
                newDoc.push(pos);
            }
        }
        newDoc.sort(function(a, b) {
            let c = new Date(a.Date);
            let d = new Date(b.Date);
            return (d - c);
        });

        return newDoc.slice(0, 5);
    },
    'LastTransactionsByPeriod': function(Accounts) {
        let newDoc = [];
        Accounts = Accounts ? Accounts : [];
        for (let i = 0; i < Accounts.length; i++) {
            for (let j = 0; j < Accounts[i].Statement.length; j++) {
                let pos ={};
                pos = Accounts[i].Statement[j];
                pos['AccountNumber'] = Accounts[i].AccountId;
                pos['Amount'] = pos['Balance'] + ' ' + Accounts[i].Currency;
                if (Accounts[i].Statement[j].BookingDate) {
                let time = Accounts[i].Statement[j].BookingDate.slice(0, 10);

                    pos['Date'] = time;
                }
                newDoc.push(pos);
            }
        }
        newDoc.sort(function(a, b) {
            let c = new Date(a.Date);
            let d = new Date(b.Date);
            return (d - c);
        });
        return newDoc;
    }
});
