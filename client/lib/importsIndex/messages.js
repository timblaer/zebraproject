import '/imports/ui/messages/messages.html';
import '/imports/ui/messages/messages.js';
import '/imports/ui/messages/MessagesTopBar/MessagesTopBar.html';
import '/imports/ui/messages/MessagesTopBar/MessagesTopBar.js';
import '/imports/ui/messages/MessagesList/MessagesList.html';
import '/imports/ui/messages/MessagesList/SingleMessage/SingleMessage.html';
import '/imports/ui/messages/ContactsList/ContactsList.html';
import '/imports/ui/messages/ContactsList/SingleContact/SingleContact.html';
