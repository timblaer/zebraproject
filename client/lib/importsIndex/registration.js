import '/imports/ui/registration/registration.css';


import '/imports/ui/registration/login/login.html';
import '/imports/ui/registration/login/login.js';

import '/imports/ui/registration/otp/otp.html';
import '/imports/ui/registration/otp/otp.js';

import '/imports/ui/registration/resetPassword/resetPassword.html';
import '/imports/ui/registration/resetPassword/resetPassword.js';

import '/imports/ui/registration/changePassword/changePassword.html';
import '/imports/ui/registration/changePassword/changePassword.js';
