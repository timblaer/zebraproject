import '/imports/ui/forms/Forms.html';
import '/imports/ui/forms/Forms.js';

import '/imports/ui/forms/FormsInputDatepicker/FormsInputDatepicker.html';
import '/imports/ui/forms/FormsInputDatepicker/FormsInputDatepicker.js';

import '/imports/ui/forms/FormsInputBasic/FormsInputBasic.html';
import '/imports/ui/forms/FormsInputBasic/FormsInputBasic.js';
import '/imports/ui/forms/FormsInputBasic/FormsInputBasic.css';

import '/imports/ui/forms/FormsInputTextarea/FormsInputTextarea.html';

import '/imports/ui/forms/FormsInputSelect/FormsInputSelect.html';
import '/imports/ui/forms/FormsInputSelect/FormsInputSelect.js';
import '/imports/ui/forms/FormsInputSelect/FormsInputSelect.css';

import '/imports/ui/forms/FormsInputTags/FormsInputTags.html';
import '/imports/ui/forms/FormsInputTags/FormsInputTags.js';

import '/imports/ui/forms/FormsInputFileInput/FormsInputFileInput.html';
import '/imports/ui/forms/FormsInputFileInput/FormsInputFileInput.js';

import '/imports/ui/forms/FormsInputCheckbox/FormsInputCheckbox.html';
import '/imports/ui/forms/FormsInputCheckbox/FormsInputCheckbox.js';

import '/imports/ui/forms/FormsButtons/ResetForm/ResetForm.html';
import '/imports/ui/forms/FormsButtons/SubmitForm/SubmitForm.html';
