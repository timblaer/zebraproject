import '/imports/ui/dataTable/dataTable.html';
import '/imports/ui/dataTable/dataTable.js';
import '/imports/ui/dataTable/InfoTable/InfoTable.html';
import '/imports/ui/dataTable/DT.html';
import '/imports/ui/forms/FormsDataTable/CheckBox/CheckBox.html';
import '/imports/ui/forms/FormsDataTable/CheckBox/CheckBox.js';

import '/imports/ui/simpleTable/simpleTable.html';
import '/imports/ui/simpleTable/simpleTable.js';
import '/imports/ui/simpleTable/ResponsiveTable/responsiveTable.html';
import '/imports/ui/simpleTable/ResponsiveTable/responsiveTable.js';
import '/imports/ui/dataTable/InfoTable/InfoTable.js';


import '/imports/ui/dataTable/dataTable.css';
import '/imports/ui/dataTable/InfoTable/InfoTable.css';
