import {get, isNull} from 'lodash';
import {mongoParse} from '/imports/libs/Utils';

const getStorage = (prop) => {
    switch (prop) {
        case 'user.sessionID':
        case 'user.login':
        case 'user.cusIdHash':
        case 'user.approvalCode':
        case 'user.cryptoKey':
        case 'user.personalData':
        case 'user.otp':
        case 'user.errorCode':
            return window.sessionStorage;
        case 'Dictionary.CurrentLanguage':
            return window.localStorage;
        default:
            return null;
    }
};

AppStorage = class AppStorage {
    static set(prop, value) {
        const params = prop.split(':');
        const storage = getStorage(params[0]);

        if (!isNull(storage)) {
            storage.setItem(params[0], value);
        }
    }

    static get(prop) {
        const storage = getStorage(prop);
        const value   = !isNull(storage) ? storage.getItem(prop) : null;
        return mongoParse(value);
    }

    static clear() {
        window.sessionStorage.clear();
        Router.go('login');
    }
};
