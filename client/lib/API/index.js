API = class API {
    static login({login, password}) {
        return Meteor.callAsync(
            'API.login',
            {login, password}
        );
    }

    static getDecryptKey({otp, sessionId}) {
        return Meteor.callAsync(
            'API.getDecryptKey',
            {otp, sessionId}
        );
    }

    static getPersonalData({sessionId, approvalCode}) {
        return Meteor.callAsync(
            'API.getPersonalData',
            {sessionId, approvalCode}
        );
    }

    static forgetPassword({login}) {
        return Meteor.callAsync(
            'API.forgetPassword',
            {login}
        );
    }

    static changePassword({sessionId, cusIdHash, password}) {
        return Meteor.callAsync(
            'API.changePassword',
            {sessionId, cusIdHash, password}
        );
    }

    static requestOtp({sessionId, login}) {
        return Meteor.callAsync(
            'API.requestOtp',
            {sessionId, login}
        );
    }
};
