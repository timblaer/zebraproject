import {isEmpty, isUndefined, get} from 'lodash';

/*
value: //some value`
reactive: true/false

*/

const LOG = false;

class State {
    constructor() {
        const self = this;

        self.state     = new Mongo.Collection(null);
        self.callbacks = [];

        this.state.find({}).observeChanges({
            'added': self.callback.bind(self), // why bind? (-t check this please)
            'changed': self.callback.bind(self),
            'removed': self.callback.bind(self)
        });
    }

    callback(id) {
        this.callbacks.forEach((x) => x(id));
    }

    set(prop, value, isReactive = false) {
        const state = this;

        if (LOG) {
if (get(value, 'ready', null) == false) {
                state.log(`${prop} not ready`);
            } else if (get(value, 'ready', null) == true) {
                state.log(`${prop} is ready`);
            }
}

        if (value instanceof AppQuery) {
            Tracker.autorun((computation) => {
                if (computation.firstRun || state.isReactive(prop)) {
                    state.set(prop, value.processData(), true);
                }
                else {
                    computation.stop();
                }
            });
        }
        else {
            this.state.upsert(prop, {
                value,
                isReactive
            });
        }
    }

    get(prop) {
        const stateData = this.state.find({_id: prop}).fetch();

        return get(stateData[0], 'value', null);
    }

    listen() {
        return this.state.find({});
    }

    isReactive(prop) {
        const stateData = this.state.find({_id: prop}).fetch();

        return get(stateData[0], 'isReactive', false);
    }

    remove(prop) {
        this.state.remove(prop);
    }

    isReady() {
        const readyState = this.state.find({'value.ready': true});
        const mayBeReady = this.state.find({'value.ready': {$ne: true, $exists: true}});

        return readyState.count() < mayBeReady.count() ? false : true;
    }

    setReady(prop, ready) {
        this.set(prop, {ready});
    }

    clear() {
        this.state.remove({});
    }

    addCallback(callback) {
        this.callbacks.push(callback);
    }
}

AppState = new State();
