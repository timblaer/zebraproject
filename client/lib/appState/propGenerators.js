const PAGINATION_NAME = 'pagination';
const DATATABLE_NAME  = 'datatable';

export const paginationToState = (component, prop) => `${PAGINATION_NAME}.${prop}.${component._id}`;
export const dataTableToState  = (component, prop) => `${DATATABLE_NAME}.${prop}.${component._id}`;

export const componentIsReady = (component) => `${component.parent}.${component.name}.isReady`;
