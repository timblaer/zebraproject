import {get} from 'lodash';
import Utils from '/imports/libs/Utils';

Template.registerHelper(
    'AppStorage_get',
    (prop) => Utils.dataGetter(prop, AppStorage)
);

Template.registerHelper(
    'AppState_get',
    (prop) => Utils.dataGetter(prop, AppState)
);

Template.registerHelper(
    'AppState_listen',
    (prop) => AppState.listen() // ? check this
);
