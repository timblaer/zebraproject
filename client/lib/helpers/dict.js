import Utils from '/imports/libs/Utils';
window.u = Utils;
Template.registerHelper('Dict', (prop) => {
    const CurrentLang = AppState.get('Dictionary.CurrentLanguage');
    return Utils.dataGetter(`Dictionary:${prop}:${CurrentLang}`, AppState);
});
