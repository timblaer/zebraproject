import {get} from 'lodash';

Template.registerHelper('Router_isCurrentRoute', (route) => {
    const currentRoute = Router.current().route && Router.current().route.getName();

    return route == currentRoute || currentRoute.indexOf(route) == 0;
});

Template.registerHelper('Router_getCurrentRoute', (prop) => {
    const currentRoute = Router.current().route && Router.current().route.getName();

    return get(Config.findOne('routes'), `${currentRoute}.${prop}`, null);
});
