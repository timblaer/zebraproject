import Utils from '/imports/libs/Utils';
import {get} from 'lodash';

Template.registerHelper('Utils_untemplateString', (string) => {
    return Utils.untemplateString(string);
});

Template.registerHelper(
    'Utils_mongoParse',
    (x) => Utils.mongoParse(x)
);

Template.registerHelper(
    'Utils_get',
    (obj, props, defaultValue = null) => get(obj, props, defaultValue)
);
