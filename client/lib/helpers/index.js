import {get, isEmpty} from 'lodash';
import {packRows} from '/imports/libs/Utils';
import {page_content_onresize, onload} from '/client/lib/actions';

Template.registerHelper('Container_getWidgets', (containerName) => {
    const widgets = Widgets.find({parent: containerName}).fetch();

    return packRows(widgets);
});

Template.registerHelper('MainLayout_loaded', () => {
    onload();
});

Template.registerHelper('Widget_getComponent', (componentName) => {
    const widget     = Template.instance().data;
    const components = get(Template.instance().data, `settings.components`, []);

    const componentData = components.find((x) => x.name === componentName);

    if (!isEmpty(widget)) {
return {
            ...componentData,
            parent: widget.name
        };
}
});
