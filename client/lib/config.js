export const Components = {
    blankComponent:             "blankComponent",
    calendar:                   "calendar",
    DataTable:                  "DataTable",
    Forms:                      "Forms",
    FormsInputBasic:            "FormsInputBasic",
    FormsInputCheckbox:         "FormsInputCheckbox",
    FormsInputDatepicker:       "FormsInputDatepicker",
    FormsInputFileInput:        "FormsInputFileInput",
    FormsInputSelect:           "FormsInputSelect",
    FormsInputTags:             "FormsInputTags",
    FormsInputTextarea:         "FormsInputTextarea",
    MessageBox:                 "MessageBox",
    MessageBoxSingleMessage:    "MessageBoxSingleMessage",
    Messages:                   "Messages",
    ContactsList:               "ContactsList",
    SingleContact:              "SingleContact",
    MessagesList:               "MessagesList",
    MessagesTopBar:             "MessagesTopBar",
    SimpleTable:                "SimpleTable",
    ResponsiveTable:            "ResponsiveTable",
    SingleRangeSlider:          "SingleRangeSlider",
    SingleKnobSlider:           "SingleKnobSlider",
    SingleIonRangeSlider:       "SingleIonRangeSlider"
}

export function AppComponentID() {
    const args        = [...arguments];
    const component   = args[0];
    const postfix     = args.slice(1);

    return `${component.type}.${component.parent}.${component.index}.${postfix.join(".")}`;
}
