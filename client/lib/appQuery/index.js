import {isEmpty, get} from 'lodash';
import {Collections} from '/lib/collections';

AppQuery = class AppQuery {
    constructor(query) {
        const
            collection  = get(query, 'collection', null),
            method      = get(query, 'method', null),
            body        = get(query, 'body', {}),
            additional  = get(query, 'additional', {});
        this.props       = get(query, 'props', null);
        this.process     = get(query, 'process', null);
        this.data   = Collections[collection] ? Collections[collection].find(body, additional) : null;
        this.one    = method == 'findOne';
    }

    isOne() {
        return this.one;
    }

    getValue() {
        const fetched = !isEmpty(this.data) ? this.data.fetch() : null;

        if (this.one) {
            const single = fetched[0];

            if (isEmpty(this.props)) {
                return single;
            }
            else {
                return get(single, this.props, single);
            }
        }

        return fetched;
    }

    processData() {
        if (!isEmpty(this.process) && this.one) {
            return AppEvents.run(this.process, this.getValue());
        }


        return this.getValue();
    }
};
