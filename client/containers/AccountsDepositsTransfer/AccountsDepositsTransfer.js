Template.AccountsDepositsTransfer.onCreated(function() {
    AppState.set('Container_AccountsDepositsTransfer', {ready: false});
});

Template.AccountsDepositsTransfer.onRendered(function() {
    AppState.set('Container_AccountsDepositsTransfer', {ready: true});
    window.scroll(0, 0);
});
