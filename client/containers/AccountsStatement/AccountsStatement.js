Template.AccountsStatement.onCreated(function() {
    AppState.set('Container_AccountsStatement', {ready: false});
});

Template.AccountsStatement.onRendered(function() {
    AppState.set('Container_AccountsStatement', {ready: true});
    window.scroll(0, 0);
});

Template.AccountsStatement.onDestroyed(function() {
    AppState.remove('CurrentStatement');
});
