Template.Cards.onCreated(function() {
    AppState.set('Container_Cards', {ready: false});
});

Template.Cards.onRendered(function() {
    AppState.set('Container_Cards', {ready: true});
    window.scroll(0, 0);
});
