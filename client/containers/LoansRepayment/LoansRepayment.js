Template.LoanPayment.onCreated(function() {
    AppState.set('Container_LoansRepayment', {ready: false});
});

Template.LoanPayment.onRendered(function() {
    AppState.set('Container_LoansRepayment', {ready: true});
    window.scroll(0, 0);
});

Template.LoanPayment.onDestroyed(function() {
    AppState.remove('CurrentLoan');
});
