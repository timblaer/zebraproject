Template.LoanPayment.onCreated(function() {
    AppState.set('Container_LoanPayment', {ready: false});
});

Template.AccountsStatement.onRendered(function() {
    AppState.set('Container_LoanPayment', {ready: true});
    window.scroll(0, 0);
});

Template.AccountsStatement.onDestroyed(function() {
    AppState.remove('CurrentLoan');
});
