Template.Loans.onCreated(function() {
    AppState.set('Container_Loans', {ready: false});
});

Template.Loans.onRendered(function() {
    AppState.set('Container_Loans', {ready: true});
    window.scroll(0, 0);
});
