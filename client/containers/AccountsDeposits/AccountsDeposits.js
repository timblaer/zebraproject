Template.AccountsDeposits.onCreated(function() {
    AppState.set('Container_AccountsDeposits', {ready: false});
});

Template.AccountsDeposits.onRendered(function() {
    AppState.set('Container_AccountsDeposits', {ready: true});
    window.scroll(0, 0);
});
