Template.ProfileContainer.onCreated(function() {
    AppState.set('Container_Profile', {ready: false});
});

Template.ProfileContainer.onRendered(function() {
    AppState.set('Container_Profile', {ready: true});
    window.scroll(0, 0);
});
