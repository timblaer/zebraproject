Template.AccountsCurrentAccounts.onCreated(function() {
    AppState.set('Container_AccountsCurrentAccounts', {ready: false});
});

Template.AccountsCurrentAccounts.onRendered(function() {
    AppState.set('Container_AccountsCurrentAccounts', {ready: true});
    window.scroll(0, 0);
});
