import {get}        from 'lodash';
import {packRows}   from '/imports/libs/Utils';

Template.Dashboard.onRendered(function() {
    window.scroll(0, 0);
});

Template.Dashboard.helpers({
    rows() {
        const dashboard = Dashboards.findOne();
        const widgets = AppState.get('Dashboard.edit') ? AppState.get('Dashboard.currentWidgets') : get(dashboard, 'widgets', []);

        return packRows(widgets.map((name) => Widgets.findOne({name})));
    },

    miniWidgets() {
        const dashboard = Dashboards.findOne();
        return get(dashboard, 'miniWidgets', []);
    }
});

Template.Dashboard.onDestroyed(function() {
    AppState.set('Dashboard.edit', false);
});
