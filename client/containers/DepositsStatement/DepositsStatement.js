Template.DepositCurrent.onCreated(function() {
    AppState.set('Container_DepositCurrentStatement', {ready: false});
});

Template.DepositCurrent.onRendered(function() {
    AppState.set('Container_DepositCurrentStatement', {ready: true});
    window.scroll(0, 0);
});

Template.DepositCurrent.onDestroyed(function() {
    AppState.remove('CurrentDeposit');
});
