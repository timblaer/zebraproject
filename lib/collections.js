Dashboards  = new Mongo.Collection('dashboards');
Widgets     = new Mongo.Collection('widgets');
Config      = new Mongo.Collection('config');
Dictionary  = new Mongo.Collection('dictionary');

const Remote = DDP.connect('http://94.130.104.207:5432');

Remote.subscribe('files');
Remote.subscribe('ProductList');
Remote.subscribe('GeneralData');

Files = new Mongo.Collection('files', {
  connection: Remote
});

ProductList = new Mongo.Collection('bes.ProductList', {
  connection: Remote
});

GeneralData = new Mongo.Collection('bes.GeneralData', {
  connection: Remote
});

export const Collections = {
    GeneralData, ProductList, Files, Config, Dictionary
};
