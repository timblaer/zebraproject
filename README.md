
```
========== ========== ==========  ==========      ======
      ===  ==         ==       == ==       ==    ==    ==
   ===     =======    =========   =========     ==========  
 ===       ==         ==       == ==     ==    ==        ==
=========  ========== ==========  ==      === ==          ==
```

ZEBRA Online Banking Platform
=============================

REQUIREMENTS
------------

* Meteor framework (>=1.5)
* NodeJS (>=5.5)

INSTALLATION
------------

1. Install [Meteor](https://www.meteor.com/) framework
    * For Linux/macOS run following command in your terminal

        ```
        curl https://install.meteor.com | sh
        ```

2. Run Meteor app

    ```
    meteor
    ```

3. Restore database from last backup using this command
    ```
    .backups/setup_db.sh -d meteor -p 3001
    ```

    If an error occured
    * Make sure that all \*.sh files in .backups directory are executable
    * Make sure that Meteor app is running

4. Run
    ```
    meteor npm install
    ```

ESLINT INSTALLATION
-------------------
We use ESLint to improve our JS Code.

1. Install ESLint package for Atom: ```apm install eslint```
2. Install Linter-ESLint package for Atom: ```apm install linter-eslint```

BUILT WITH
----------

* Meteor 1.5
* Bootstrap 3.x

BRANCHES
--------
* Master

ANY QUESTIONS LEFT?
-------------------

Feel free to ask team in Telegram chat
